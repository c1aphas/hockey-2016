#!/usr/bin/env node
require('../server.babel');
var path = require('path');
process.chdir('./src');

var module = path.resolve(process.cwd(), './scraper/' + process.argv[2] + '.js');

global.__SERVER__ = true;

console.time('task');
require(module).run().then(
  function(result) {
    console.log(result);
    console.timeEnd('task');
    process.exit(0);
  },
  function(error) {
    console.error(error);
    process.exit(1);
  }
);
