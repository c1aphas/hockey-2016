var bootstrap = require('rambler-bootstrap');
var gulp = require('gulp');

gulp.task('build:icons', function() {
  return gulp.src('src')
    .pipe(bootstrap.icons({
      distDir: 'static/svg',
      iconsFilter: [
        '!**',
        'video-full.svg',
        'arrows/*.svg',
        'logo.svg',
        'social/social-*',
        'attention.svg',
        'quote.svg',
        'championat.svg'
      ],
      iconsExtra: [
        'static/images/*.svg'
      ]
    }));
});

