## Installation

```bash
npm install
```



## Running Dev Server

Create `src/config/credentials.development.json` file and fill out api credentials:
```
{
  "apiAuth": "username:password"
}
```

```bash
npm run dev
```

[http://localhost:3000/hockey-2016/](http://localhost:3000/hockey-2016/)

The first time it may take a little while to generate the first `webpack-assets.json` and complain with a few dozen `[webpack-isomorphic-tools] (waiting for the first Webpack build to finish)` printouts, but be patient. Give it 30 seconds.


## Building and Running Production Server

process.env.HOST should contain correct hostname
```bash
npm run build
npm run start
```

## Reading list
* [React official docs](https://facebook.github.io/react/docs/getting-started.html)
* [Redux](http://redux.js.org/)
* [Smart and Dumb components](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0#.osqmj6f8a)
* [ES6, ES2015 Cheatsheet](https://github.com/DrkSephy/es6-cheatsheet)
