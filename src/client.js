import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import createStore from './redux/create';
import ApiClient from './helpers/ApiClient';
import {Provider} from 'react-redux';
import { Router, browserHistory } from 'react-router';
import { ReduxAsyncConnect } from 'redux-async-connect';
import withScroll from 'scroll-behavior/lib/withStandardScroll';
import getRoutes from './routes';
import moment from 'moment-timezone';

moment.tz.setDefault('Europe/Moscow');

const client = new ApiClient();
const history = withScroll(browserHistory,
  (oldLocation, newLocation) => {
    // better to use scroll.lock and scroll.release
    // const offsetTop = document.getElementById('app').offsetTop - 20;
    if (!oldLocation) {
      return false;
    }
    if (newLocation && oldLocation && oldLocation.pathname === newLocation.pathname) {
      return false;
    }
    return [0, 0];
  });

const dest = document.getElementById('content');
const store = createStore(history, client, window.__data);

function renderRouter(props) {
  return (
    <ReduxAsyncConnect
      {...props}
      helpers={{client}}
      filter={item => !item.deferred}
    />
  );
}

const component = (
  <Router render={renderRouter} history={history}>
    {getRoutes(store)}
  </Router>
);

ReactDOM.render(
  <Provider store={store} key="provider">
    {component}
  </Provider>,
  dest
);

if (process.env.NODE_ENV !== 'production') {
  window.React = React; // enable debugger

  if (!dest || !dest.firstChild || !dest.firstChild.attributes || !dest.firstChild.attributes['data-react-checksum']) {
    console.error('Server-side React render was discarded. Make sure that your initial render does not contain any client-side code.');
  }
}
