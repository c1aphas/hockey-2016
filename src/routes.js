import config from 'config';
import React from 'react';
import { IndexRoute, Route, Router } from 'react-router';
import * as containers from 'containers';

export default () => {
  return (
    <Router>
      <Route path={`${config.app.rootPath}informer/`} component={containers.EmptyLayout}>
        <Route path="desktop" component={containers.InformerDesktop} />
        <Route path="mobile" component={containers.InformerMobile} />
      </Route>

      <Route path={config.app.rootPath} component={containers.App}>
        <IndexRoute component={containers.Home} />

        { /* '/articles': 301 to '/' */ }
        <Route path="articles/:request" component={containers.Publication} />
        <Route path="news" component={containers.NewsList} />
        <Route path="news/:request" component={containers.Publication} />
        <Route path="raspisanie" component={containers.Schedule} />
        <Route path="raspisanie/:id/:date/:teams" component={containers.Match} />
        <Route path="turnirnaya-tablica" component={containers.Standings} />

        <Route path="*" component={containers.NotFound} status={404} />
      </Route>
    </Router>
  );
};
