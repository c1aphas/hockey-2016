import Express from 'express';
import React from 'react';
import ReactDOM from 'react-dom/server';
import config from './config';
import favicon from 'serve-favicon';
import compression from 'compression';
import path from 'path';
import createStore from './redux/create';
import ApiClient from './helpers/ApiClient';
import ApiProxy from './helpers/ApiProxy';
import Html from './helpers/Html';
import PrettyError from 'pretty-error';
import http from 'http';

import { match } from 'react-router';
import { ReduxAsyncConnect, loadOnServer } from 'redux-async-connect';
import createHistory from 'react-router/lib/createMemoryHistory';
import {Provider} from 'react-redux';
import getRoutes from './routes';
import NestedStatus from 'react-nested-status';

const pretty = new PrettyError();
const app = new Express();
const server = new http.Server(app);

app.use(compression());

app.use(config.app.rootPath, Express.static(path.join(__dirname, '..', 'static')));

if (config.setupApiProxy) {
  new ApiProxy(app);
}

app.use((req, res) => {
  if (__DEVELOPMENT__) {
    // Do not cache webpack stats: the script file would change since
    // hot module replacement is enabled in the development env
    webpackIsomorphicTools.refresh();
  }
  const client = new ApiClient(req);
  const history = createHistory(req.originalUrl);

  const store = createStore(history, client);

  function hydrateOnClient() {
    res.send('<!doctype html>\n' +
      ReactDOM.renderToString(<Html assets={webpackIsomorphicTools.assets()} store={store}/>));
  }

  if (__DISABLE_SSR__) {
    hydrateOnClient();
    return;
  }

  match({ history, routes: getRoutes(store), location: req.originalUrl }, (error, redirectLocation, renderProps) => {
    if (redirectLocation) {
      res.redirect(redirectLocation.pathname + redirectLocation.search);
    } else if (error) {
      console.error('ROUTER ERROR:', pretty.render(error));
      res.status(500);
      hydrateOnClient();
    } else if (renderProps) {
      loadOnServer({...renderProps, store, helpers: {client}}).then(() => {
        const component = (
          <Provider store={store} key="provider">
            <ReduxAsyncConnect {...renderProps} />
          </Provider>
        );

        global.navigator = {userAgent: req.headers['user-agent']};
        global.isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(req.headers['user-agent']);

        const page = <Html assets={webpackIsomorphicTools.assets()} component={component} store={store} location={req.originalUrl} />;
        const html = '<!doctype html>\n' + ReactDOM.renderToString(page);

        global.navigator = {userAgent: req.headers['user-agent']};

        res.status(NestedStatus.rewind() || 200);
        res.send(html);
      });
    } else {
      res.status(404).send('Not found');
    }
  });
});

if (config.port) {
  server.listen(config.port, (err) => {
    if (err) {
      console.error(err);
    }
    console.info('----\n==> ✅  sport.rambler.ru/hockey-2016/ is running');
    if (__DEVELOPMENT__) {
      console.info(
        '==> 💻  Open http://%s:%s%s in a browser to view the app.',
        config.api_host, config.port, config.app.rootPath,
      );
    } else {
      console.info(
        '==> 💻  Open http://%s%s in a browser to view the app.',
        config.api_host, config.app.rootPath,
      );
    }

  });
} else {
  console.error('==>     ERROR: No PORT environment variable has been specified');
}
