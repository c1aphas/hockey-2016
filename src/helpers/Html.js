import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import ReactDOM from 'react-dom/server';
import serialize from 'serialize-javascript';
import Helmet from 'react-helmet';
import { YaCounter, TnsCounter, CriteoCounter, BodyCounters } from 'components';
import { toggleEmbedMode } from 'redux/modules/app';
import { URL_RAMBLER_SPORT } from 'helpers/Constants';

@connect(
  state => ({
    isEmbed: state.app.isEmbed
  }), {...{toggleEmbedMode: toggleEmbedMode}}
)
export default class Html extends Component {
  static propTypes = {
    assets: PropTypes.object,
    component: PropTypes.node,
    store: PropTypes.object
  };

  render() {
    const isEmbed = !!~['/hockey-2016/informer/desktop', '/hockey-2016/informer/mobile'].indexOf(this.props.location);
    this.props.toggleEmbedMode(isEmbed);
    const {assets, component, store} = this.props;
    const content = component ? ReactDOM.renderToString(component) : '';
    const head = Helmet.rewind();
    const prodStyles = !__DEVELOPMENT__ ? <link href={assets.styles.entry} type="text/css" rel="stylesheet" /> : null;
    const renderTypes = {
      'full':
        <html lang="ru">
          <head>
            {head.base.toComponent()}
            {head.title.toComponent()}

            {head.meta.toComponent()}
            <meta charSet="utf-8" />
            <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
            <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=620" />

            {prodStyles}
            <script src={assets.javascript.entry} charSet="UTF-8"></script>

            {head.link.toComponent()}
            {head.script.toComponent()}
            <script src="//mc.yandex.ru/metrika/watch.js"></script>
            <YaCounter />
            <CriteoCounter />
            <TnsCounter />
            <script type='text/javascript' dangerouslySetInnerHTML={{
                __html: `(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                         m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                         })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                         ga('create', 'UA-15822767-18', 'auto');
                         ga('send', 'pageview');`
              }}></script>
          </head>
          <body>
            <div id="svg-sprite" style={{display: 'none'}}></div>

            <div id="content" dangerouslySetInnerHTML={{__html: content}}></div>
            <script dangerouslySetInnerHTML={{__html: `window.__data=${serialize(store.getState())};`}} charSet="UTF-8"></script>

            <BodyCounters />
            <script src="//autocontext.begun.ru/autocontext2_async.js" type="text/javascript"></script>
            <script src="//lib.rl0.ru/adfox/7/adfox.custom.min.js" type="text/javascript"></script>
            <script>
              if (window.Adf) Adf.init.begun();
              window.ads = [];
            </script>
          </body>
        </html>,
      'light':
        <html lang="ru" className="mobile">
          <head>
            {head.base.toComponent()}
            <meta charSet="utf-8" />
            <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
            <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />

            {head.link.toComponent()}
            {prodStyles}
            <script src={assets.javascript.entry} charSet="UTF-8"></script>

            {head.script.toComponent()}
          </head>
          <body>
            <div id="svg-sprite" style={{display: 'none'}}></div>

            <div id="content" dangerouslySetInnerHTML={{__html: content}}></div>
            <script dangerouslySetInnerHTML={{__html: `window.__data=${serialize(store.getState())};`}} charSet="UTF-8"></script>
          </body>
        </html>
    };
    return isEmbed ? renderTypes.light : renderTypes.full;
  }
}
