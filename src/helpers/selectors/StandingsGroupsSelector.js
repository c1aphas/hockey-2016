import { createSelector } from 'reselect';
import standingsSelector from './StandingsSelector';
import { filter, transform } from 'lodash';

export default createSelector(
  standingsSelector,
  (standings) => {
    const groups = filter(standings, (item) => {
      return item.type === 'tournament.table.group';
    });
    return transform(groups, (result, item) => {
      result.push(item.data);
    });
  }
);
