import { createSelector } from 'reselect';
import standingsSelector from './StandingsSelector';
import { filter, transform } from 'lodash';

export default createSelector(
  standingsSelector,
  (standings) => {
    const playoff = filter(standings, (item) => {
      return item.type === 'tournament.table.playoff';
    });
    return transform(playoff, (result, item) => {
      result.push(item.data);
    });
  }
);
