import { createSelector } from 'reselect';

export default createSelector(
  (state) => {
    return state.schedule.data.data;
  },
  (schedule) => {
    return schedule;
  }
);
