import { createSelector } from 'reselect';
import ScheduleSelector from './ScheduleSelector';
import groupBy from 'lodash/groupBy';

export default createSelector(
  ScheduleSelector,
  (schedule) => {
    return groupBy(schedule, (item) => {
      return item.date;
    });
  }
);
