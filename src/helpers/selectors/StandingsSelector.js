import { createSelector } from 'reselect';

export default createSelector(
  (state) => {
    return state.standings.data.data;
  },
  (standings) => {
    return standings;
  }
);
