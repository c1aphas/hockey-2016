import superagent from 'superagent';
import config from '../config';

const methods = ['get', 'post', 'put', 'patch', 'del'];

function formatUrl(path) {
  const adjustedPath = path[0] !== '/' ? '/' + path : path;
  let result;
  if (__SERVER__) {
    result = config.apiEndpoint + adjustedPath;
  } else {
    result = '/hockey-2016/api/v3' + adjustedPath;
  }
  return result;
}

export default class ApiClient {
  constructor(req) {
    methods.forEach((method) =>
      this[method] = (path, { params, data } = {}) => new Promise((resolve, reject) => {
        const request = superagent[method](formatUrl(path));

        if (params) {
          request.query(params);
        }

        if (__SERVER__ && req.get('cookie')) {
          request.set('cookie', req.get('cookie'));
        }

        if (data) {
          request.send(data);
        }

        request.end((err, { body } = {}) => {
          //console.log(formatUrl(path), err, body);
          return err ? reject(body || err) : resolve(body);
        });
      }));
  }
  // https://phabricator.babeljs.io/T2455
  derp() {}
}
