let lastScroll = {x: null, y: null};

function init() {
  lastScroll = {x: null, y: null};
}

export default class Scroll {
  static scrollLock() {
    if (window) {
      lastScroll.x = window.scrollX;
      lastScroll.y = window.scrollY;
    }
  }

  static scrollRelease() {
    if (window && lastScroll.x !== null && lastScroll.y !== null) {
      window.scrollTo(lastScroll.x, lastScroll.y);
      init();
    }
  }
}
