export const MATCH_STATE_NO_OPPONENTS = 'ntl'; // соперники не определены
export const MATCH_STATE_NOTSTARTED = 'dns'; // не начался
export const MATCH_STATE_POSTPONED = 'post'; // перенесен
export const MATCH_STATE_PAUSE = 'delay'; // остановлен
export const MATCH_STATE_1T = '1t'; // 1-й период
export const MATCH_STATE_2T = '2t'; // 2-й период
export const MATCH_STATE_3T = '3t'; // 3-й период
export const MATCH_STATE_HALF = 'half'; // перерыв
export const MATCH_STATE_OVERTIME = 'extra'; // овертайм
export const MATCH_STATE_BULLITS = 'pen'; // буллиты
export const MATCH_STATE_FINISHED = 'fin'; // окончен

export const MATCH_STATES_ONLINE = [
  MATCH_STATE_PAUSE,
  MATCH_STATE_1T,
  MATCH_STATE_2T,
  MATCH_STATE_3T,
  MATCH_STATE_HALF,
  MATCH_STATE_OVERTIME,
  MATCH_STATE_BULLITS
];
