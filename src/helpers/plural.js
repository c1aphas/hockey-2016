export default function plural(word, num) {
  const forms = word.split('_');
  let result;
  if (num % 10 === 1 && num % 100 !== 11) {
    result = forms[0];
  } else {
    if (num % 10 >= 2 && num % 10 <= 4 && (num % 100 < 10 || num % 100 >= 20)) {
      result = forms[1];
    } else {
      result = forms[2];
    }
  }
  return result;
}
