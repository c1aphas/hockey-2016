import lodash from 'lodash';

export const COUNTRY_NAME_RUS = 'Россия';
export const COUNTRY_NAME_SWE = 'Швеция';
export const COUNTRY_NAME_CZE = 'Чехия';
export const COUNTRY_NAME_SUI = 'Швейцария';
export const COUNTRY_NAME_LAT = 'Латвия';
export const COUNTRY_NAME_NOR = 'Норвегия';
export const COUNTRY_NAME_DEN = 'Дания';
export const COUNTRY_NAME_KAZ = 'Казахстан';
export const COUNTRY_NAME_CAN = 'Канада';
export const COUNTRY_NAME_FIN = 'Финляндия';
export const COUNTRY_NAME_USA = 'США';
export const COUNTRY_NAME_SVK = 'Словакия';
export const COUNTRY_NAME_BLR = 'Беларусь';
export const COUNTRY_NAME_FRA = 'Франция';
export const COUNTRY_NAME_GER = 'Германия';
export const COUNTRY_NAME_HUN = 'Венгрия';
export const COUNTRY_NAME_AUS = 'Австрия';
export const COUNTRY_NAME_SLO = 'Словения'; //2015


export const COUNTRY_CODE_RUS = 'RUS';
export const COUNTRY_CODE_SWE = 'SWE';
export const COUNTRY_CODE_CZE = 'CZE';
export const COUNTRY_CODE_SUI = 'SUI';
export const COUNTRY_CODE_LAT = 'LAT';
export const COUNTRY_CODE_NOR = 'NOR';
export const COUNTRY_CODE_DEN = 'DEN';
export const COUNTRY_CODE_KAZ = 'KAZ';
export const COUNTRY_CODE_CAN = 'CAN';
export const COUNTRY_CODE_FIN = 'FIN';
export const COUNTRY_CODE_USA = 'USA';
export const COUNTRY_CODE_SVK = 'SVK';
export const COUNTRY_CODE_BLR = 'BLR';
export const COUNTRY_CODE_FRA = 'FRA';
export const COUNTRY_CODE_GER = 'GER';
export const COUNTRY_CODE_HUN = 'HUN';
export const COUNTRY_CODE_AUS = 'AUS';
export const COUNTRY_CODE_SLO = 'SLO';  //2015


export const COUNTRY_SLUG_RUS = 'russia';
export const COUNTRY_SLUG_SWE = 'sweden';
export const COUNTRY_SLUG_CZE = 'czech_republic';
export const COUNTRY_SLUG_SUI = 'switzerland';
export const COUNTRY_SLUG_LAT = 'latvia';
export const COUNTRY_SLUG_NOR = 'norway';
export const COUNTRY_SLUG_DEN = 'denmark';
export const COUNTRY_SLUG_KAZ = 'kazakhstan';
export const COUNTRY_SLUG_CAN = 'canada';
export const COUNTRY_SLUG_FIN = 'finland';
export const COUNTRY_SLUG_USA = 'usa';
export const COUNTRY_SLUG_SVK = 'slovakia';
export const COUNTRY_SLUG_BLR = 'belarus';
export const COUNTRY_SLUG_FRA = 'france';
export const COUNTRY_SLUG_GER = 'germany';
export const COUNTRY_SLUG_HUN = 'hungary';
export const COUNTRY_SLUG_AUS = 'austria';  //2015
export const COUNTRY_SLUG_SLO = 'slovenia'; //2015


const nameToCode = {
  [COUNTRY_NAME_RUS]: COUNTRY_CODE_RUS,
  [COUNTRY_NAME_SWE]: COUNTRY_CODE_SWE,
  [COUNTRY_NAME_CZE]: COUNTRY_CODE_CZE,
  [COUNTRY_NAME_SUI]: COUNTRY_CODE_SUI,
  [COUNTRY_NAME_LAT]: COUNTRY_CODE_LAT,
  [COUNTRY_NAME_NOR]: COUNTRY_CODE_NOR,
  [COUNTRY_NAME_DEN]: COUNTRY_CODE_DEN,
  [COUNTRY_NAME_KAZ]: COUNTRY_CODE_KAZ,
  [COUNTRY_NAME_CAN]: COUNTRY_CODE_CAN,
  [COUNTRY_NAME_FIN]: COUNTRY_CODE_FIN,
  [COUNTRY_NAME_USA]: COUNTRY_CODE_USA,
  [COUNTRY_NAME_SVK]: COUNTRY_CODE_SVK,
  [COUNTRY_NAME_BLR]: COUNTRY_CODE_BLR,
  [COUNTRY_NAME_FRA]: COUNTRY_CODE_FRA,
  [COUNTRY_NAME_GER]: COUNTRY_CODE_GER,
  [COUNTRY_NAME_HUN]: COUNTRY_CODE_HUN,
  [COUNTRY_NAME_AUS]: COUNTRY_CODE_AUS, //2015
  [COUNTRY_NAME_SLO]: COUNTRY_CODE_SLO  //2015
};

const nameToSlug = {
  [COUNTRY_NAME_RUS]: COUNTRY_SLUG_RUS,
  [COUNTRY_NAME_SWE]: COUNTRY_SLUG_SWE,
  [COUNTRY_NAME_CZE]: COUNTRY_SLUG_CZE,
  [COUNTRY_NAME_SUI]: COUNTRY_SLUG_SUI,
  [COUNTRY_NAME_LAT]: COUNTRY_SLUG_LAT,
  [COUNTRY_NAME_NOR]: COUNTRY_SLUG_NOR,
  [COUNTRY_NAME_DEN]: COUNTRY_SLUG_DEN,
  [COUNTRY_NAME_KAZ]: COUNTRY_SLUG_KAZ,
  [COUNTRY_NAME_CAN]: COUNTRY_SLUG_CAN,
  [COUNTRY_NAME_FIN]: COUNTRY_SLUG_FIN,
  [COUNTRY_NAME_USA]: COUNTRY_SLUG_USA,
  [COUNTRY_NAME_SVK]: COUNTRY_SLUG_SVK,
  [COUNTRY_NAME_BLR]: COUNTRY_SLUG_BLR,
  [COUNTRY_NAME_FRA]: COUNTRY_SLUG_FRA,
  [COUNTRY_NAME_GER]: COUNTRY_SLUG_GER,
  [COUNTRY_NAME_HUN]: COUNTRY_SLUG_HUN,
  [COUNTRY_NAME_AUS]: COUNTRY_SLUG_AUS, //2015
  [COUNTRY_NAME_SLO]: COUNTRY_SLUG_SLO  //2015
};

export function getCountryName(code) {
  return lodash.capitalize(
    Object.keys(nameToCode)
      .find(key => nameToCode[key] === code.toUpperCase())
  );
}

export function getCountryCode(name) {
  return lodash.mapKeys(
    nameToCode,
    (value, key) => key.toLowerCase()
  )[name.toLowerCase()];
}

export function getCountrySlug(name) {
  return lodash.mapKeys(
    nameToSlug,
    (value, key) => key.toLowerCase()
  )[name.toLowerCase()];
}
