import httpProxy from 'http-proxy';
import config from 'config';

export default class ApiProxy {
  constructor(app) {
    const proxy = httpProxy.createProxyServer({
      target: config.apiProxyEndpoint,
      auth: config.apiAuth,
      changeOrigin: true
    });

    app.use('/hockey-2016/api', (req, res) => {
      proxy.web(req, res);
    });

    proxy.on('error', (error, req, res) => {
      if (error.code !== 'ECONNRESET') {
        console.error('proxy error', error);
      }

      if (!res.headersSent) {
        res.writeHead(500, {'content-type': 'application/json'});
      }

      let json = {error: 'proxy_error', reason: error.message};
      res.end(JSON.stringify(json));
    });
  }
  // https://phabricator.babeljs.io/T2455
  derp() {}
}