import { writeFileSync } from 'fs';
import ApiClient from '../helpers/ApiClient';
import config from '../config';

export async function run() {
  const teams = await (new ApiClient).get(`/sports/hockey/tournaments/${config.app.tournamentId}/teams/`);

  const dict = {};

  teams.data
    .map(t => t.players)
    .reduce((cur, next) => cur.concat(next), [])
    .filter(t => t.photo)
    .forEach(t => dict[t.player_id] = t.photo);

  writeFileSync('../static/data/player-photo.json', JSON.stringify(dict));

  return Promise.resolve('Done');
}
