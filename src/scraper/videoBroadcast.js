import { readFileSync, writeFileSync } from 'fs';
import moment from 'moment';

export async function run() {
  const items = readFileSync('../static/data/video-broadcast.csv', 'utf-8')
    .split("\n")
    .map(row => row.replace(/\s+/g, ''))
    .filter(row => row)
    .map(row => {
      const data = row.split(',');
      return {
        start: moment(`2016-${data[0]}T${data[1]}`).valueOf(),
        end  : moment(`2016-${data[2]}T${data[3]}`).valueOf(),
        team1: data[4],
        team2: data[5]
      }
    })
    .sort((a, b) => a.start - b.start);

  writeFileSync('../static/data/video-broadcast.json', JSON.stringify(items));

  return Promise.resolve('Done');
}
