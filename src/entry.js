require('base.scss');
require('utils/timersProxy.js');

if (document.getElementById('svg-sprite')) {
  require('inline?parentId=svg-sprite!../static/svg/sprite.svg');
} else {
  document.addEventListener('DOMContentLoaded', function loaded() {
    require('inline?parentId=svg-sprite!../static/svg/sprite.svg');
  });
}

let loaded = false;

document.addEventListener('DOMContentLoaded', function contentLoaded() {
  loaded = true;
});

require.ensure(['./client.js', './utils/adfoxLoader.js'], function ensured() {
  if (loaded === true) {
    require('./client.js');
    return;
  }
  document.addEventListener('DOMContentLoaded', function contentLoaded() {
    require('./client.js');
  });
}, 'bundle');
