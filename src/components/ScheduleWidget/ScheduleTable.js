import React, { Component, PropTypes } from 'react';
import { Flag, MatchLink } from '../';
import { getCountryCode } from 'helpers/Countries';
import map from 'lodash/map';
import classNames from 'classnames';
import {
  MATCH_STATE_NO_OPPONENTS, MATCH_STATE_NOTSTARTED, MATCH_STATES_ONLINE,
  MATCH_STATE_1T, MATCH_STATE_2T, MATCH_STATE_3T
} from 'helpers/MatchStates';

export default class extends Component {
  static propTypes = {
    matches: PropTypes.array.isRequired
  };
  getStatus(rawStatus) {
    if (MATCH_STATES_ONLINE.indexOf(rawStatus) !== -1) {
      return 'live';
    } else if (~[MATCH_STATE_NOTSTARTED, MATCH_STATE_NO_OPPONENTS].indexOf(rawStatus)) {
      return 'scheduled';
    }

    return 'past';
  }
  getScore(match) {
    switch (this.getStatus(match.raw_status)) {
      default:
        return match.result.detailed.goal1 + ':' + match.result.detailed.goal2;
      case 'scheduled':
        return '–:–';
    }
  }
  getTextStatus(match) {
    const lives = {
      [MATCH_STATE_1T]: 'I ПЕРИОД',
      [MATCH_STATE_2T]: 'II ПЕРИОД',
      [MATCH_STATE_3T]: 'III ПЕРИОД'
    };

    switch (this.getStatus(match.raw_status)) {
      case 'past':
        return 'ЗАВЕРШЕН';
      case 'scheduled':
        return match.time;
      case 'live':
        return typeof lives[match.raw_status] !== 'undefined' ? lives[match.raw_status] : match.status;
      default:
        return match.status;
    }
  }
  generateClassNames(rawStatus) {
    const status = this.getStatus(rawStatus);
    return classNames(
      'schedule-table__match',
      {
        'schedule-table__match--past': status === 'past',
        'schedule-table__match--scheduled': status === 'scheduled',
        'schedule-table__match--live': status === 'live'
      }
    );
  }
  generateName(name) {
    return getCountryCode(name) || name;
  }
  generateGroup(match) {
    return match.group.name;
  }
  render() {
    const { matches } = this.props;
    return (
      <div className="schedule-table__matches-wrapper">
        <table className="schedule-table__matches">
          <tbody>
            {map(matches, (match, matchI) =>
              <MatchLink match={match} tag="tr" className={this.generateClassNames(match.raw_status)} key={matchI}>
                <td className="schedule-table__match-status" dangerouslySetInnerHTML={{__html: this.getTextStatus(match)}}></td>
                <td className="schedule-table__match-side1">{this.generateName(match.teams[0].name)}</td>
                <td className="schedule-table__match-flag1">
                  <Flag nameFull={match.teams[0].name} />
                </td>
                <td className="schedule-table__match-score">
                  <div className="schedule-table__match-score-text">{this.getScore(match)}</div>
                </td>
                <td className="schedule-table__match-flag2">
                  <Flag nameFull={match.teams[1].name} />
                </td>
                <td className="schedule-table__match-side2">{this.generateName(match.teams[1].name)}</td>
                <td className="schedule-table__match-group">{this.generateGroup(match)}</td>
              </MatchLink>
            )}
          </tbody>
        </table>
      </div>
    );
  }
}
