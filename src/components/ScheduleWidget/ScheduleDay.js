import React, { Component, PropTypes } from 'react';
import ScheduleTable from './ScheduleTable';
import moment from 'moment';
import { KLink } from 'components';

export default class extends Component {
  static propTypes = {
    day: PropTypes.string.isRequired,
    matches: PropTypes.array.isRequired
  };

  render() {
    // visible-large hidden-medium hidden-small
    const {day, matches} = this.props;
    const matches1 = matches.slice(0, Math.ceil(matches.length / 2));
    const matches2 = matches.slice(Math.ceil(matches.length / 2));
    const weekday = moment(day).format('dddd');
    const date = moment(day).format('D MMMM');

    return (
      <div className="schedule-table">
        <KLink to="raspisanie" className="schedule-table__header top-slider__header">РАСПИСАНИЕ
          <div className="schedule-table__header-weekday">{weekday}</div>
          <div className="schedule-table__header-date">{date}</div>
        </KLink>
        <div className="schedule-table__content">
          <ScheduleTable matches={matches1}/>
          <ScheduleTable matches={matches2}/>
        </div>
        <div className="schedule-table__delim"></div>
      </div>
    );
  }
}
