import React, { Component, PropTypes } from 'react';
import ScheduleDay from './ScheduleDay';
import { Icon, Carousel } from 'components';
import classNames from 'classnames';
import { size, forEach, map, keys } from 'lodash';
import moment from 'moment';

export default class extends Component {
  static propTypes = {
    schedule: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.maxSlide = size(this.props.schedule);
    this.todayIndex = this.calcTodayIndex();
    this._setArrowClassNames(this.todayIndex);
  }
  state = {
    leftClassNames: '',
    rightClassNames: '',
    carousel: null
  }
  onSlideChange(slideIdx) {
    this._setArrowClassNames(slideIdx);
    this.setState({
      leftClassNames: this.state.leftClassNames,
      rightClassNames: this.state.rightClassNames
    });
  }
  setCarousel(carousel) {
    this.state.carousel = carousel;
  }
  _setArrowClassNames(slideIdx) {
    this.state.leftClassNames = classNames({
      'top-slider__arrow': true,
      'top-slider__arrow--disabled': slideIdx === 0
    });
    this.state.rightClassNames = classNames({
      'top-slider__arrow': true,
      'top-slider__arrow--disabled': slideIdx === this.maxSlide - 1
    });
  }
  previousSlide() {
    this.state.carousel.previousSlide();
  }
  nextSlide() {
    this.state.carousel.nextSlide();
  }
  calcTodayIndex() {
    const today = moment();
    const todayStr = today.format('YYYY-MM-DD');

    const days = keys(this.props.schedule);
    const firstDay = moment(days[0], 'YYYY-MM-DD');
    const lastDay = moment(days[days.length - 1], 'YYYY-MM-DD');

    if (today.isBetween(firstDay, lastDay)) {
      const diff = days.indexOf(todayStr);
      if (diff !== -1) {
        return diff;
      }

      let currDay = firstDay;
      forEach(days, (dayStr) => {
        currDay = moment(dayStr, 'YYYY-MM-DD');
        if (currDay.isAfter(today)) {
          return false;
        }
      });
      return days.indexOf(currDay.format('YYYY-MM-DD'));
    } else if (today.isAfter(lastDay)) {
      return days.length - 1;
    }

    return 0;
  }

  render() {
    const { schedule } = this.props;
    const carouselSettings = {
      onSlideChange: this.onSlideChange.bind(this),
      setCarousel: this.setCarousel.bind(this),
      nukaCarouselSettings: {
        slideIndex: this.todayIndex
      }
    };
    return (
      <section className="schedule top-slider">
        <div className="schedule__content-wrapper">
          <div className="schedule__content">
            <Carousel {...carouselSettings} key="schedule">
              {map(schedule, (matches, day) =>
                <ScheduleDay key={day} day={day} matches={matches}/>
              )}
            </Carousel>
          </div>
          <div className="top-slider__arrows">
            <div className={this.state.leftClassNames} onClick={this.previousSlide.bind(this)}>
              <Icon name="angle-left"/>
            </div>
            <div className={this.state.rightClassNames} onClick={this.nextSlide.bind(this)}>
              <Icon name="angle-right"/>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

