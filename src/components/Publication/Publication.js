import React, { Component, PropTypes } from 'react';
import * as con from 'helpers/Constants';
import * as moment from 'moment';
import { KLink, SocialButtons, Icon, Ad } from 'components';

export default class Publication extends Component {
  static propTypes = {
    pub: PropTypes.object
  };

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  flattenContent(content) {
    return content.map((block, idx) => {
      const type = block.type || Object.keys(block)[0];
      const val = block[type];
      return {type, val, idx};
    });
  }

  bannerIndex = 0;

  blocks = {
    text: (val, idx) => <div className="article-content__text" dangerouslySetInnerHTML={{ __html: val }} key={idx}></div>,
    paragraph: () => null,
    banner: (val, idx) => {
      this.bannerIndex++;
      return (<Ad
        id="inread"
        mobileId="inpage"
        index={this.bannerIndex}
        mobileIndex={this.bannerIndex}
        key={idx}
      />);
    },
    quote: (val, idx) => (
      <div className="article-content__quote" key={idx}>
        <Icon name="quote" class="article-content__quote-icon" />
        <div className="article-content__quote-content" dangerouslySetInnerHTML={{ __html: val }}></div>
      </div>
    )
  };

  renderBlock(block) {
    const renderer = this.blocks[block.type];
    return renderer ? renderer(block.val, block.idx) : null;
  }

  renderRelated(links) {
    if (!links || !links.length) {
      return null;
    }

    return (
      <div className="read-more article__read-more">
        <div className="read-more__title">
          Читайте также на Championat.com
        </div>
        <div className="read-more__list">
          {links.map((link, idx) =>
            <a href={`${con.URL_WWW_CHAMPIONAT}/${link.link}`} target="_blank" key={idx} className="read-more__item">
              {link.title}
            </a>
          )}
        </div>
      </div>
    );
  }

  render() {
    const pub = this.props.pub;
    return (
      <div className="article">
        <div className="article__header article-header">
          <div className="article-header__top">
            <KLink to="/" className="article-header__top-back">
              На главную
            </KLink>
            <div className="article-header__top-date">
              {moment.unix(pub.pub_date).format('dddd D MMMM, HH:mm')}
            </div>
          </div>

          <div className="article-header__title">
            {pub.title}
          </div>

          {pub.author &&
            <div className="article-header__author">
              Автор {pub.author.name}
            </div>
          }
        </div>

        <div className="article__content article-content">
          {pub.subtitle &&
            <div className="article-content__subtitle">
              {pub.subtitle}
            </div>
          }

          {pub.image &&
            <div className="article-content__inner--image">
              <img className="article-content__image" src={`${con.URL_IMG_CHAMPIONAT}/${pub.image.url}`} alt={pub.image.title} />
              <div className="article-content__caption--photo">Фото: {pub.image.author}</div>
            </div>
          }

          {this.flattenContent(pub.content).map(this.renderBlock.bind(this))}

          {pub.author &&
            <div className="article-header__author">
              Автор {pub.author.name}
            </div>
          }
        </div>

        <SocialButtons classes="article__social" />

        {this.renderRelated(pub.more_links)}
      </div>
    );
  }
}
