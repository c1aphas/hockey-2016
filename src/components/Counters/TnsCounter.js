import React, { Component } from 'react';

export default class extends Component {
  render() {
    return (
      <script type="text/javascript" dangerouslySetInnerHTML={{
        __html:
          `(function(win, doc, cb){
            (win[cb] = win[cb] || []).push(function() {
              try {
                tnsCounterRambler_ru = new TNS.TnsCounter({
                'account':'rambler_ru',
                'tmsec': 'rambler_sport'
                });
              } catch(e){}
            });

            var tnsscript = doc.createElement('script');
            tnsscript.type = 'text/javascript';
            tnsscript.async = true;
            tnsscript.src = ('https:' == doc.location.protocol ? 'https:' : 'http:') +
              '//www.tns-counter.ru/tcounter.js';
            var s = doc.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(tnsscript, s);
          })(window, this.document,'tnscounter_callback');`
      }}></script>
    );
  }
}
