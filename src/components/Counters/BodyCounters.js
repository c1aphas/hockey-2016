import React, { Component } from 'react';

export default class extends Component {
  render() {
    return (
      <div className="counters">
        <script type="text/javascript" dangerouslySetInnerHTML={{
          __html: `var _top100q = _top100q || [];
                _top100q.push(['setAccount', '164843']);
                _top100q.push(['trackPageviewByLogo', document.getElementById('Rambler-counter')]);
                (function(){
                  var pa = document.createElement("script");
                  pa.type = "text/javascript";
                  pa.async = true;
                  pa.src = ("https:" == document.location.protocol ? "https:" : "http:") + "//st.top100.ru/top100/top100.js";
                  var s = document.getElementsByTagName("script")[0];
                  s.parentNode.insertBefore(pa, s);
                })();`
        }}></script>
        <script src="//montblanc.rambler.ru/static/js/montblanc.js"></script>
        <script src="//cntcerber.rambler.ru/counter.js"></script>
        <script type="text/javascript" dangerouslySetInnerHTML={{
          __html: `window.cerberCounter = new CerberCounter({
                    project: 'rambler.sport',
                    attributes_dataset: [
                        'cerber-topline'
                    ]
                  });`
        }}></script>
        <noscript><img src="//cntcerber.rambler.ru/noscript/rambler.head/?.v.js=off" /></noscript>
        <div id="Rambler-counter">
          <noscript>
            <a href="http://top100.rambler.ru/navi/164843/">
              <img src="http://counter.rambler.ru/top100.cnt?164843" alt="Rambler's Top100" border="0" />
            </a>
          </noscript>
        </div>
        <noscript><img src="//mc.yandex.ru/watch/26649402" style={{position: 'absolute', left: '-9999px%'}} alt="" /></noscript>
        <noscript><img src="//mc.yandex.ru/watch/36855860" style={{position: 'absolute', left: '-9999px%'}} alt="" /></noscript>
      </div>
    );
  }
}
