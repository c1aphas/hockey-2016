import React, { Component } from 'react';
import map from 'lodash/map';

export default class extends Component {
  getCounter(yaCounterParams) {
    return (
      <script type="text/javascript" dangerouslySetInnerHTML={{
        __html: map(yaCounterParams, (item) =>
          `try {
              window.yaCounter` + item.id + ` = new Ya.Metrika({
                  id:` + item.id + `,
                  clickmap:true,
                  trackLinks:true,
                  accurateTrackBounce:true,
                  webvisor:` + item.webvisor + `
              });
          } catch(e) { }`).join(' ')
      }}></script>
    );
  }
  render() {
    const yaCounterParams = [
      {
        id: '26649402',
        webvisor: 'false'
      },
      {
        id: '36855860',
        webvisor: 'true'
      }
    ];
    return this.getCounter(yaCounterParams);
  }
}
