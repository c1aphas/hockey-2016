import React, { Component } from 'react';
import { Icon, KLink } from 'components';

export default class extends Component {
  render() {
    return (
      <footer className="footer">
        <div className="footer__inner">
          <div className="footer__line footer__line_first">
            <KLink className="footer__logo" to="/">
              <Icon name="logo"/>
            </KLink>
            <div className="footer__nav">
              <KLink className="footer__nav-item" to="/">Главная</KLink>
              <KLink className="footer__nav-item" to="/raspisanie">Расписание</KLink>
              <KLink className="footer__nav-item" to="/turnirnaya-tablica">Турнирное положение</KLink>
              <KLink className="footer__nav-item" to="/news">Новости</KLink>
            </div>
          </div>
          <div className="footer__line footer__line_second">
            <div className="footer__menu">
              <div className="footer__menu-line">
                <a href="//rambler-co.ru" rel="nofollow" target="_blank" className="footer__menu-item">© Рамблер, 2016</a>
                <a href="//help.rambler.ru/legal/1430/" rel="nofollow" target="_blank" className="footer__menu-item">Правила пользования и конфиденциальность</a>
                <a href="//help.rambler.ru/feedback/hockey/" rel="nofollow" target="_blank" className="footer__menu-item">Обратная связь</a>
                <a href="//rambler-co.ru/jobs" rel="nofollow" target="_blank" className="footer__menu-item">Вакансии</a>
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}
