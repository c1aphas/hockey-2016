import React, { Component, PropTypes } from 'react';
import { KLink } from 'components';
import map from 'lodash/map';
import * as moment from 'moment';

export default class extends Component {
  static propTypes = {
    news: PropTypes.array
  };

  render() {
    const latestNews = this.props.news;
    return (
      <div className="latest-news">
        <div className="latest-news__caption">
          <div className="latest-news__caption-text">Новости</div>
        </div>
        <div className="latest-news__inner">
          <div className="latest-news__list">
            {map(latestNews, (item) =>
              <KLink className="latest-news__item" key={item.id} to={`/news/${item.internal_link}`}>
                <div className="latest-news__date">
                  {moment.unix(item.pub_date).format('dddd, HH:mm')}
                </div>
                <div className="latest-news__title">{item.title}</div>
              </KLink>
            )}
          </div>
        </div>
        <div className="latest-news__button">
          <KLink className="button button--white button--blue-hover " to="/news">Все новости</KLink>
        </div>
      </div>
    );
  }
}
