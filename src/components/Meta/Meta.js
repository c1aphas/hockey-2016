import React, { Component, PropTypes } from 'react';
import Helmet from 'react-helmet';
import config from 'config';
import { URL_RAMBLER_SPORT } from 'helpers/Constants';

export default class extends Component {
  static propTypes = {
    data: PropTypes.object,
    class: PropTypes.string
  };

  render() {
    const data = this.props.data;

    const faviconPath = config.app.rootPath + 'images/favicon';
    const canonical = `${URL_RAMBLER_SPORT}${data.pathname || config.rootpath}`;
    const title = data.title || config.app.title;

    const meta = [
      { property: 'og:title', content: data.ogTitle },
      { property: 'og:url', content: canonical },
      { name: 'description', content: data.description },
      { property: 'og:description', content: data.ogDescription },
      { property: 'og:type', content: data.ogType },
      { property: 'og:locale', content: data.ogLocale || 'ru_RU' },
      { name: 'keywords', content: data.keywords },
      { name: 'robots', content: data.robots || 'noindex, follow' }
    ].filter(t => t.content);

    if (data.image) {
      meta.push({ property: 'og:image', content: data.image });
    } else {
      meta.push({ property: 'og:image', content: `${URL_RAMBLER_SPORT}${config.app.rootPath}images/share-default.jpg` });
      meta.push({ property: 'og:image:width', content: '1200' });
      meta.push({ property: 'og:image:height', content: '630' });
    }

    const link = [
      { rel: 'canonical', href: canonical },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Roboto+Slab:400,700&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic', type: 'text/css'},
      { rel: 'apple-touch-icon', sizes: '57x57', href: faviconPath + '/apple-touch-icon-57x57.png' },
      { rel: 'apple-touch-icon', sizes: '60x60', href: faviconPath + '/apple-touch-icon-60x60.png' },
      { rel: 'apple-touch-icon', sizes: '72x72', href: faviconPath + '/apple-touch-icon-72x72.png' },
      { rel: 'apple-touch-icon', sizes: '76x76', href: faviconPath + '/apple-touch-icon-76x76.png' },
      { rel: 'apple-touch-icon', sizes: '114x114', href: faviconPath + '/apple-touch-icon-114x114.png' },
      { rel: 'apple-touch-icon', sizes: '120x120', href: faviconPath + '/apple-touch-icon-120x120.png' },
      { rel: 'apple-touch-icon', sizes: '144x144', href: faviconPath + '/apple-touch-icon-144x144.png' },
      { rel: 'apple-touch-icon', sizes: '152x152', href: faviconPath + '/apple-touch-icon-152x152.png' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: faviconPath + '/apple-touch-icon-180x180.png' },
      { rel: 'icon', type: 'image/png', href: faviconPath + '/favicon-32x32.png', sizes: '32x32' },
      { rel: 'icon', type: 'image/png', href: faviconPath + '/android-chrome-192x192.png', sizes: '192x192' },
      { rel: 'icon', type: 'image/png', href: faviconPath + '/favicon-96x96.png', sizes: '96x96' },
      { rel: 'icon', type: 'image/png', href: faviconPath + '/favicon-16x16.png', sizes: '16x16' },
      { rel: 'manifest', href: faviconPath + '/manifest.json' },
      { rel: 'mask-icon', href: faviconPath + '/safari-pinned-tab.svg', color: '#315efb' }
    ];

    return <Helmet title={title} meta={meta} link={link} />;
  }
}
