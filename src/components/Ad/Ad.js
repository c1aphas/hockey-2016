import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import Adv from 'utils/adfoxLoader';
import find from 'lodash/find';
import isMobile from 'helpers/isMobile';

export default class Ad extends Component {
  static propTypes = {
    id: PropTypes.string,
    mobileId: PropTypes.string,
    index: PropTypes.number,
    mobileIndex: PropTypes.number,
    type: PropTypes.string,
    className: PropTypes.string,
    scroll: PropTypes.bool,
    debug: PropTypes.bool
  };

  static defaultProps = {
    index: 0,
    mobileIndex: 0,
    debug: false,
    className: 'ad'
  };

  componentWillMount() {
    this.mobile = isMobile();
  }

  componentDidMount() {
    if (!window.Adf || find(window.ads, {placeholderID: this.getBannerId()})) {
      return;
    }

    if (!this.isValid()) {
      return;
    }
    // type - for mobile puid49 specific values only
    const index = this.getIndex();
    const { type, scroll } = this.props;
    const bannerProps = {
      placeholderID: this.getBannerId(),
      scroll: scroll
    };
    if (this.mobile && (index || type)) {
      bannerProps.adfoxParams = {
        puid49: type || this.getBannerId().slice(6)
      };
    }

    window.ads.push(new Adv(this.getBannerName(), bannerProps));
  }

  shouldComponentUpdate() {
    return false;
  }

  componentWillUnmount() {
    const me = ReactDOM.findDOMNode(this);
    window.ads = window.ads.filter(ad => ad.$placeholder !== me);
  }

  getIndex() {
    const { index, mobileIndex } = this.props;
    return this.mobile ? mobileIndex : index;
  }

  getBannerId() {
    const index = this.getIndex();
    const { mobileId, id } = this.props;
    const indexAddon = index ? `-${index}` : '';
    return this.mobile
      ? `adfox-${mobileId + indexAddon}`
      : `adfox-${id + indexAddon}`;
  }
  getBannerName() {
    const mobileId = this.props.mobileId;
    let id = this.props.id;
    // TODO refactor me
    if (id === 'inread') {
      id += '-' + this.props.index;
    }
    return (this.mobile && mobileId) ? mobileId : id;
  }

  getClassname() {
    let className = this.props.className;
    const debug = this.props.debug;
    if (debug) {
      className += ' ad--debug';
    }
    return className;
  }

  isValid() {
    return (this.mobile && this.props.mobileId) || (!this.mobile && this.props.id);
  }

  render() {
    return this.isValid()
      ? <div id={this.getBannerId()} className={this.getClassname()}></div>
      : null;
  }
}
