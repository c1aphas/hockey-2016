import React, { Component } from 'react';

export default class extends Component {
  componentDidMount() {
    window.ramblerToplineParams = {
      projectName: 'hockey',
      showLogo: true,
      showMobileTopline: true,
      breakpoints: {
        viewports: {
          large: 1240,
          medium: 940,
          small: 620
        },
        widths: {
          large: 1200,
          medium: 900,
          small: 580
        }
      }
    };
    const script = document.createElement('script');
    script.id = 'rambler-topline-injector';
    script.src = '//topline.rambler.ru/new/latest/bundle.js';
    // old browsers support
    script.async = true;

    document.querySelector('.nav__topline').appendChild(script);
  }

  render() {
    return (
      <div className="nav__topline"></div>
    );
  }
}
