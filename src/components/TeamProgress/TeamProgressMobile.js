import React, { Component, PropTypes } from 'react';
import find from 'lodash/find';

export default class TeamProgress extends Component {
  static propTypes = {
    data: PropTypes.array,
    team: PropTypes.string,
    stages: PropTypes.array,
    isMobile: PropTypes.bool
  };

  groupStage(data) {
    if (data.progress.game === 7) {
      return this.cupStage(data);
    }

    const stats = data.progress;

    return (
      <div className="rambler-hockey-informer-mobile-stats__values">
        <span className="rambler-hockey-informer-mobile-stats__label">И</span>
        <span className="rambler-hockey-informer-mobile-stats__value">{stats.game}</span>

        <span className="rambler-hockey-informer-mobile-stats__label">В</span>
        <span className="rambler-hockey-informer-mobile-stats__value">{stats.win}</span>

        <span className="rambler-hockey-informer-mobile-stats__label">П</span>
        <span className="rambler-hockey-informer-mobile-stats__value">{stats.lose}</span>

        <span className="rambler-hockey-informer-mobile-stats__label">О</span>
        <span className="rambler-hockey-informer-mobile-stats__value rambler-hockey-informer-mobile-stats__value--red">{stats.points}</span>
      </div>
    );
  }

  cupStage(current) {
    const winner = current.progress.winner === this.props.team;

    let resp;
    if (!current.isCup) {
      resp = current.progress.n < 5 ? 'Вышла в 1/4' : 'Не вышла из группы';
    } else if (current.stage.indexOf('1/4') > -1) {
      resp = winner ? 'Играет в полуфиналe' : 'Проиграла в 1/4';
    } else if (current.stage.indexOf('1/2') > -1) {
      resp = winner ? 'Играет в финале' : 'Играет за 3е место';
    } else if (current.stage.indexOf('3-е') > -1) {
      resp = `Заняла ${winner ? '3' : '4'}е место`;
    } else if (current.stage.toLowerCase() === 'финал') {
      resp = `Заняла ${winner ? '1' : '2'}е место`;
    }

    return <span className="rambler-hockey-informer-mobile-stats__info">{resp}</span>;
  }

  render() {
    const teamName = this.props.team;

    function groupMatched(entry) {
      return entry.name === teamName;
    }

    function playoffMatched(entry) {
      return entry.team && entry.team.length && (entry.team[0].team1 === teamName || entry.team[0].team2 === teamName);
    }

    function entryMatched(entry) {
      return groupMatched(entry) || playoffMatched(entry);
    }

    function participate(stage) {
      const yep = find(stage.data.data, entryMatched);
      if (yep) {
        return {
          stage: stage.data.name,
          isCup: stage.data.is_cup,
          progress: yep
        };
      }
    }

    const stages = this.props.data.map(participate).filter(s => s);

    const group = stages[0];
    const playoffs = stages.slice(0);
    const completedPlayoffs = playoffs.filter(stage => stage.progress.played);

    if (!group && !playoffs.length) {
      return null;
    }

    const layout = completedPlayoffs.length ? this.cupStage(completedPlayoffs.pop()) : this.groupStage(group);

    return <div className="rambler-hockey-informer-mobile-stats">{layout}</div>;
  }
}
