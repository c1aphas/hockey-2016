import React, { Component, PropTypes } from 'react';
import find from 'lodash/find';
import { Flag, KLink } from 'components';
import { getCountryCode } from 'helpers/Countries';

export default class TeamProgress extends Component {
  static propTypes = {
    data: PropTypes.array,
    team: PropTypes.string,
    stages: PropTypes.array
  };

  groupStage(data) {
    if (data.progress.game === 7) {
      return this.cupStage(data);
    }

    const stats = data.progress;

    return (
      <div className="rambler-hockey-informer-stats__values">
        <span className="rambler-hockey-informer-stats__label">Игр</span>
        <span className="rambler-hockey-informer-stats__value">{stats.game}</span>

        <span className="rambler-hockey-informer-stats__label">Выиграно</span>
        <span className="rambler-hockey-informer-stats__value">{stats.win}</span>

        <span className="rambler-hockey-informer-stats__label">Проиграно</span>
        <span className="rambler-hockey-informer-stats__value">{stats.lose}</span>

        <span className="rambler-hockey-informer-stats__label">Очков</span>
        <span className="rambler-hockey-informer-stats__value rambler-hockey-informer-stats__value--red">{stats.points}</span>
      </div>
    );
  }

  cupStage(current) {
    const winner = current.progress.winner === this.props.team;

    let resp;
    if (!current.isCup) {
      resp = current.progress.n < 5 ? 'Вышла в плей-офф, играет в четвертьфинале' : 'Не вышла из группы';
    } else if (current.stage.indexOf('1/4') > -1) {
      resp = winner ? 'Выиграла в четвертьфинале, играет в полуфиналe' : 'Проиграла в четвертьфинале';
    } else if (current.stage.indexOf('1/2') > -1) {
      resp = winner ? 'Выиграла в полуфинале, играет в финале' : 'Проиграла в полуфинале, играет за 3е место';
    } else if (current.stage.indexOf('3-е') > -1) {
      resp = `Заняла ${winner ? '3' : '4'}е место`;
    } else if (current.stage.toLowerCase() === 'финал') {
      resp = `Заняла ${winner ? '1' : '2'}е место`;
    }

    return <div className="rambler-hockey-informer-stats__info">{resp}</div>;
  }

  render() {
    const teamName = this.props.team;

    function groupMatched(entry) {
      return entry.name === teamName;
    }

    function playoffMatched(entry) {
      return entry.team && entry.team.length && (entry.team[0].team1 === teamName || entry.team[0].team2 === teamName);
    }

    function entryMatched(entry) {
      return groupMatched(entry) || playoffMatched(entry);
    }

    function participate(stage) {
      const yep = find(stage.data.data, entryMatched);
      if (yep) {
        return {
          stage: stage.data.name,
          isCup: stage.data.is_cup,
          progress: yep
        };
      }
    }

    const stages = this.props.data.map(participate).filter(s => s);

    const group = stages[0];
    const playoffs = stages.slice(0);
    const completedPlayoffs = playoffs.filter(stage => stage.progress.played);

    if (!group && !playoffs.length) {
      return null;
    }

    const layout = completedPlayoffs.length ? this.cupStage(completedPlayoffs.pop()) : this.groupStage(group);
    const teamCode = getCountryCode(teamName);

    return (
      <KLink className="rambler-hockey-informer-stats" to="/turnirnaya-tablica">
        <div className="rambler-hockey-informer-stats__country">
          <Flag name={teamCode} size="large" />
          {teamName}
        </div>
        {layout}
      </KLink>
    );
  }
}
