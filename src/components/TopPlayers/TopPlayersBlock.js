import React, { Component, PropTypes } from 'react';
import TopPlayersGoalpass from './TopPlayersGoalpass';
import TopPlayersAssistant from './TopPlayersAssistant';
import TopPlayersGoalkeeper from './TopPlayersGoalkeeper';
import * as constants from 'helpers/Constants';
import { Icon } from 'components';

export default class TopPlayersBlock extends Component {
  static propTypes = {
    data: PropTypes.object
  };

  getPlayers(section) {
    try {
      return this.props.data[section].players.data;
    } catch (e) {
      return [];
    }
  }

  render() {
    const goalpass = this.getPlayers('goalpass');
    const assistant = this.getPlayers('assistent');
    const keeper = this.getPlayers('keeper');

    if (!goalpass.length && !assistant.length && !keeper.length) {
      return null;
    }

    return (
      <div className="top-players">
        <h2 className="top-players__header">
          Лучшие игроки
          <span className="top-players__header-note">
            <a href={constants.URL_WWW_CHAMPIONAT} target="_blank"><Icon name="championat" />Чемпионат</a>
          </span>
        </h2>

        <div className="top-players__content">
          <TopPlayersGoalpass players={goalpass} />
          <div className="top-players__delim"></div>
          <TopPlayersAssistant players={assistant} />
          <div className="top-players__delim"></div>
          <TopPlayersGoalkeeper players={keeper} />
        </div>
      </div>
    );
  }
}
