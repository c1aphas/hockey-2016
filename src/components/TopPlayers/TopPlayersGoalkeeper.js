import React from 'react';
import { Flag } from 'components';
import TopPlayersBase from './TopPlayersBase';

export default class TopPlayersGoalkeeper extends TopPlayersBase {
  title = 'Вратари';

  classes = 'top-players-type--goalkeepers';

  firstStats() {
    const player = this.getFirstPlayer();

    return (
      <div className="top-players-type__first-stats top-players-first-stats">
        {this.firstStatsEntry('ПРОПУЩЕНО ШАЙБ', 'goalsAgainst', player.data)}
        {this.firstStatsEntry('СРЕДНЕЕ', 'keeperAssists', player.average)}
        {this.firstStatsEntry('ИГРЫ', 'games', player.game)}
      </div>
    );
  }

  underdogsStats() {
    const players = this.getUnderdogs();

    return (
      <div className="top-players-type__stats top-players-stats">
        <table className="top-players-stats__table">
          <tbody>
          <tr>
            <th colSpan="2"></th>
            <th>П</th>
            <th>С</th>
            <th>И</th>
          </tr>

          {players.map(player => (
            <tr className="top-players-stats__row" key={`${this.classes}-${player.player_id}`}>
              <td className="top-players-stats__flag">
                <Flag nameFull={player.country[0].name} />
              </td>
              <td className="top-players-stats__name">
                {player.player}
              </td>
              <td className="top-players-stats__value">
                {player.data}
              </td>
              <td className="top-players-stats__value">
                {player.average}
              </td>
              <td className="top-players-stats__value">
                {player.game}
              </td>
            </tr>
          ))}
          </tbody>
        </table>
      </div>
    );
  }
}
