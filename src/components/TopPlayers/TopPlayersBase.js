import React, { Component, PropTypes } from 'react';
import { Flag } from 'components';
import { getCountryCode } from 'helpers/Countries';
import photos from '../../../static/data/player-photo.json';
import * as constants from 'helpers/Constants';

export default class TopPlayersBase extends Component {
  static propTypes = {
    players: PropTypes.array
  };

  getFirstPlayer() {
    return this.props.players[0];
  }

  getUnderdogs() {
    return this.props.players.slice(1, 6);
  }

  firstStatsEntry(title, slug, value) {
    return (
      <div className={`top-players-first-stats__item top-players-first-stats__item--${slug}`}>
        <div className="top-players-first-stats__label" dangerouslySetInnerHTML={{ __html: title }}></div>
        <div className="top-players-first-stats__value">
          {value}
        </div>
      </div>
    );
  }

  first() {
    const player = this.getFirstPlayer();
    const country = player.country[0];
    const avatar = photos[player.player_id]
      ? <img src={`${constants.URL_IMG_CHAMPIONAT}/team/player/${photos[player.player_id]}`} />
      : <img src="/hockey-2016/images/player-default.png" alt="Фото отсутствует" title="Фото отсутствует" />;

    return (
      <div className="top-players-type__first-info top-players-first-info">
        <div className="top-players-first-info__photo">
          <div className="top-players-first-info__circle">
            <div className="top-players-first-info__img">
              {avatar}
            </div>
          </div>
        </div>

        <div className="top-players-first-info__right">
          <div className="top-players-first-info__name">
            {player.player}
          </div>

          <div className="top-players-first-info__flag">
            <Flag nameFull={country.name} />
            {getCountryCode(country.name)}
          </div>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className={`top-players-type ${this.classes}`}>
        <div className="top-players-type__header">
          {this.title}
        </div>

        <div className="top-players-type__container">
          {this.first()}
          {this.firstStats()}
          <div className="top-players-type__delim"></div>
          {this.underdogsStats()}
        </div>
      </div>
    );
  }
}
