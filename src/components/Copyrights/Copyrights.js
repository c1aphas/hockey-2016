import React, { Component, PropTypes } from 'react';
import map from 'lodash/map';

export default class extends Component {
  static propTypes = {
    copyrightsData: PropTypes.array
  };

  render() {
    const copyrightsData = this.props.copyrightsData;
    return (
      <div className="copyrights">
        {map(copyrightsData, (item, key) =>
          <div className="copyrights__item" key={key}>
            <h4 className="copyrights__title">{item.title}</h4>
            <div className="copyrights__text">{item.text}</div>
          </div>
        )}
      </div>
    );
  }
}
