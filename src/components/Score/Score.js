import React, { Component, PropTypes } from 'react';

export default class extends Component {
  static propTypes = {
    score: PropTypes.string
  };
  static defaultProps = {
    score: '–:–'
  };

  render() {
    const { score } = this.props.score;
    return (
      <span>{score}</span>
    );
  }
}
