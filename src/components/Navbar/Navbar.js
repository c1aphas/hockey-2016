import React, { Component, PropTypes } from 'react';
import { Topline, KLink, Icon } from 'components';
import { map } from 'lodash';
import classNames from 'classnames';

export default class extends Component {
  static propTypes = {
    location: PropTypes.object
  };
  renderLink(link, i) {
    const linkClass = classNames({
      'nav__links-item': true,
      'nav__links-item--active': link[0] === this.props.location.pathname
    });
    return (
      <KLink key={i} className={linkClass} to={link[0]}>{link[1]}</KLink>
    );
  }
  render() {
    const links = [
      ['/', 'Главная'],
      ['/raspisanie', 'Расписание'],
      ['/turnirnaya-tablica', 'Турнирное положение'],
      ['/news', 'Новости']
    ];
    return (
      <nav className="nav">
        <Topline />
        <div className="nav__sub">
          <div className="nav__inner">
            <KLink to="/" className="nav__logo">
              <Icon name="logo" />
            </KLink>
            <div className="nav__links">
              {map(links, (link, i) => this.renderLink(link, i))}
            </div>
            <div className="nav__dummy"></div>
          </div>
        </div>
      </nav>
    );
  }
}
