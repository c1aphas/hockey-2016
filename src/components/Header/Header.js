import React, { Component } from 'react';
import { AnnouncementWidget } from 'containers';
import { KLink } from 'components';

export default class extends Component {
  render() {
    return (
      <header className="header">
        <div className="header__inner">
          <div className="header__text">
            <KLink tag="div" to="/" className="header__logo">Чемпионат мира по хоккею 2016</KLink>
            <div className="header__date">6 – 22 мая</div>
          </div>
          <AnnouncementWidget />
        </div>
      </header>
    );
  }
}
