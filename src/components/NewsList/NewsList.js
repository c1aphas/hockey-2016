import React, { Component, PropTypes } from 'react';
import { KLink, Ad } from 'components';
import * as Nprogress from 'nprogress';
import groupBy from 'lodash/groupBy';
import moment from 'moment';
import classNames from 'classnames';
import map from 'lodash/map';
import Scroll from 'helpers/Scroll';

export default class NewsList extends Component {
  static propTypes = {
    news: PropTypes.array,
    loadMore: PropTypes.func,
    isLoading: PropTypes.bool
  };

  componentWillReceiveProps(nextProps) {
    Nprogress[nextProps.isLoading ? 'start' : 'done']();
  }

  shouldComponentUpdate(nextProps) {
    return !nextProps.isLoading;
  }

  componentWillUpdate() {
    Scroll.scrollRelease();
  }

  componentWillUnmount() {
    return false;
  }

  bannerLoadedTimes = 0;
  itemsLoaded = 0;
  loadmoreTimes = 0;

  groupDate(date) {
    const readable = moment.unix(date).format('D MMMM');
    const parts = readable.split(' ');

    return (
      <div className="news-feed__date">
        {parts[0]}
        <div className="news-feed-date__month">
          {parts[1]}
        </div>
      </div>
    );
  }

  showAd(idx) {
    if (this.loadmoreTimes >= 2) {
      const adPlaces = (new Array(this.loadmoreTimes - 1)).fill(0).map((v, i) => {
        return (i + 1) * 40 + 1;
      });
      if (~adPlaces.indexOf(idx)) {
        this.bannerLoadedTimes++;
        return (<Ad
          id="context"
          mobileId="listing"
          index={this.bannerLoadedTimes}
          mobileIndex={this.bannerLoadedTimes}
          scroll={false}
          key={idx}
        />);
      }
    }

    return null;
  }
  loadMore() {
    this.loadmoreTimes++;
    Scroll.scrollLock();
    this.props.loadMore();
  }

  render() {
    const news = groupBy(this.props.news, 'group_key');
    const groups = Object.keys(news).sort((a, b) => b - a);
    const buttonClassNames = classNames({
      'button button--blue button--more': true,
      'button--loading': this.props.isLoading
    });
    this.itemsLoaded = 0;
    this.bannerLoadedTimes = 0;
    return (
      <div className="news-feed">
        <div className="news-feed__inner">
          <div className="news-feed__caption">
            Все новости
          </div>
          <div className="news-feed__list">
            {map(groups, (group, groupIdx) =>
              <div className="news-feed__item" key={`newslist-group-${groupIdx}`}>
                {this.groupDate(news[group][0].pub_date)}
                <div className="news-feed__content">
                  {map(news[group], (item) =>
                    <div key={`newslist-entry-${item._id}`}>
                      {this.showAd(++this.itemsLoaded)}
                      <div className="news-feed__news">
                        <span className="news-feed__time">
                          {moment.unix(item.pub_date).format('HH:mm')}
                        </span>

                        <KLink to={`/news/${item.internal_link}`} className="news-feed__title">
                          {item.title}
                        </KLink>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            )}
          </div>
          <div className="pagination news-feed__pagination">
            <div className="pagination__inner">
              <div className="more">
                <div className={buttonClassNames} onClick={this.loadMore.bind(this)}>
                  Еще новости
                  <div className="loading-dots">
                    <div className="loading-dot"></div>
                    <div className="loading-dot"></div>
                    <div className="loading-dot"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
