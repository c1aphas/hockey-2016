import React, { Component, PropTypes } from 'react';
import { getCountryCode } from 'helpers/Countries';

export default class Flag extends Component {
  static propTypes = {
    name: PropTypes.string,
    nameFull: PropTypes.string,
    size: PropTypes.string,
    classes: PropTypes.string
  };
  static defaultProps = {
    size: 'small',
    classes: ''
  };
  render() {
    let name = '';
    if (this.props.name) {
      name = this.props.name.toLowerCase();
    } else if (this.props.nameFull) {
      name = getCountryCode(this.props.nameFull) || 'null';
    }
    name = name.toLowerCase();

    const size = this.props.size;
    const classes = this.props.classes;
    return (
      <div className={`flag flag--${name} flag--${size} ${classes}`}></div>
    );
  }
}
