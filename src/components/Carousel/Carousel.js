import React, { Component, PropTypes } from 'react';
import ReactDom from 'react-dom';
import Carousel from 'nuka-carousel';

let carouselId = 0;

export default class CarouselWrapper extends Component {
  static propTypes = {
    children: React.PropTypes.node,
    onSlideChange: PropTypes.func,
    setCarousel: PropTypes.func,
    nukaCarouselSettings: PropTypes.object
  };
  static defaultProps = {
    carouselSettings: {}
  };
  constructor(props) {
    super(props);
    this.carouselId = 'carousel' + carouselId++;
  }
  shouldComponentUpdate() { //nextProps, nextState
    return false;
  }
  onSlideChange(slideIdx) {
    this.props.onSlideChange(slideIdx);
  }
  setAutoHeight(slideIdx) {
    // auto height
    const ref = this.refs[this.carouselId];
    const dom = ReactDom.findDOMNode(ref);
    const listDom = ReactDom.findDOMNode(ref.refs.list);
    const slideHeight = listDom.childNodes[slideIdx].offsetHeight;

    dom.style.height = slideHeight + 'px';
  }
  setCarousel() {
    if (!this.refs[this.carouselId]) {
      return;
    }
    const ref = this.refs[this.carouselId];
    /* Set updateAutoHeight func */
    ref.updateAutoHeight = () => {
      this.setAutoHeight(ref.state.currentSlide);
    };

    if (this.props.setCarousel) {
      this.props.setCarousel(ref);
    }
    ref.updateAutoHeight();
  }
  render() {
    const carouselSettings = {
      ref: this.carouselId,
      decorators: [],
      data: this.setCarousel.bind(this),
      afterSlide: this.onSlideChange.bind(this),
      ...this.props.nukaCarouselSettings
    };
    return (
      <Carousel {...carouselSettings}>
        {this.props.children}
      </Carousel>
    );
  }
}

