import React, { Component, PropTypes } from 'react';
import { Flag } from '../index';
import { getCountryCode } from 'helpers/Countries';
import { map } from 'lodash';
import classNames from 'classnames';

import { connect } from 'react-redux';
import { standingsWidget } from 'redux/modules/standingsWidget';

@connect(
  state => ({
    expanded: state.standingsWidget.expanded
  }), {...{standingsWidget: standingsWidget}}
)

export default class extends Component {
  static propTypes = {
    teams: PropTypes.array.isRequired,
    expanded: PropTypes.bool
  };
  generateRowClass(team) {
    return classNames({
      'standing-table-team': true,
      'hidden': team.n > 4 && this.props.expanded !== true
    });
  }
  render() {
    const { teams } = this.props;
    return (
      <div className="standing-table__wrapper">
        <table className="standing-table">
          <thead>
            <tr className="standing-table__header">
              <th colSpan="3"></th>
              <th>И</th>
              <th>В</th>
              <th>ВО/ВБ</th>
              <th>ПО/ПБ</th>
              <th>П</th>
              <th>РШ</th>
              <th>О</th>
            </tr>
          </thead>
          <tbody>
          {map(teams, (team, idx) =>
            <tr className={this.generateRowClass(team)} key={idx}>
              <td className="standing-table-team__place">{team.n}</td>
              <td className="standing-table-team__flag">
                <Flag nameFull={team.name} />
              </td>
              <td className="standing-table-team__title">{getCountryCode(team.name)}</td>
              <td className="standing-table-team__data">{team.game}</td>
              <td className="standing-table-team__data">{team.win}</td>
              <td className="standing-table-team__data">{team.wino}/{team.winb}</td>
              <td className="standing-table-team__data">{team.loseo}/{team.loseb}</td>
              <td className="standing-table-team__data">{team.lose}</td>
              <td className="standing-table-team__data">{team.goal1 - team.goal2}</td>
              <td className="standing-table-team__score">{team.points}</td>
            </tr>
          )}
          </tbody>
        </table>
      </div>
    );
  }
}
