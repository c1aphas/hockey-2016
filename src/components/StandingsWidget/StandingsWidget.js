import React, { Component, PropTypes } from 'react';

import StandingsGroups from './StandingsGroups';
import StandingsPlayOff from './StandingsPlayOff';
import classNames from 'classnames';
import { Icon, Carousel } from 'components';

export default class extends Component {
  static propTypes = {
    groups: PropTypes.array,
    playoff: PropTypes.array
  };
  constructor(props) {
    super(props);
    this.carouselId = 'carousel';
    this._setArrowClassNames(1);
  }
  state = {
    leftClassNames: '',
    rightClassNames: '',
    carousel: null
  }
  onSlideChange(slideIdx) {
    this._setArrowClassNames(slideIdx);
    this.setState({
      leftClassNames: this.state.leftClassNames,
      rightClassNames: this.state.rightClassNames
    });
  }
  setCarousel(carousel) {
    this.state.carousel = carousel;
  }
  getCarousel() {
    return this.state.carousel;
  }
  _setArrowClassNames(slideIdx) {
    this.state.leftClassNames = classNames({
      'top-slider__arrow': true,
      'top-slider__arrow--disabled': slideIdx === 0
    });
    this.state.rightClassNames = classNames({
      'top-slider__arrow': true,
      'top-slider__arrow--disabled': slideIdx === 1
    });
  }
  previousSlide() {
    this.state.carousel.previousSlide();
  }
  nextSlide() {
    this.state.carousel.nextSlide();
  }
  render() {
    const { groups, playoff } = this.props;
    const carouselSettings = {
      onSlideChange: this.onSlideChange.bind(this),
      setCarousel: this.setCarousel.bind(this),
      nukaCarouselSettings: {
        slideIndex: 1
      }
    };
    return (
      <section className="standings top-slider">
        <div className="standings__content-wrapper">
            <Carousel {...carouselSettings} key="standings">
              <StandingsGroups groups={groups} getCarousel={this.getCarousel.bind(this)} />
              <div className="standings__content">
                <StandingsPlayOff playoff={playoff} />
              </div>
            </Carousel>
            <div className="top-slider__arrows">
              <div className={this.state.leftClassNames} onClick={this.previousSlide.bind(this)}>
                <Icon name="angle-left" />
              </div>
              <div className={this.state.rightClassNames} onClick={this.nextSlide.bind(this)}>
                <Icon name="angle-right" />
              </div>
            </div>
        </div>
      </section>
    );
  }
}
