import React, { Component, PropTypes } from 'react';
import StandingsTable from './StandingsTable';
import { KLink } from 'components';

export default class extends Component {
  static propTypes = {
    group: PropTypes.object.isRequired
  };
  render() {
    const { group } = this.props;
    return (
      <div className="standing-group">
        <KLink to="turnirnaya-tablica" className="standing-group__header top-slider__header">
          <div className="standing-group__header-left">ТУРНИРНАЯ ТАБЛИЦА</div>
          <div className="standing-group__header-right">{ group.name }</div>
        </KLink>
        <div className="standing-group__content">
          <StandingsTable teams={group.data}/>
        </div>
      </div>
    );
  }
}
