import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import { Flag, PlayOffMatchLink } from '../index';
import { getCountryCode } from 'helpers/Countries';

export default class extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    round: PropTypes.number.isRequired
  };
  // <div className={`flag flag--${name} flag--${size} ${classes}`}></div>
  getScore(match) {
    return match.played ? match.tgoal1 + ':' + match.tgoal2 : '';
  }

  render() {
    const { match, round } = this.props;
    const matchClassNames = classNames({
      'play-off-match standing-playoff__match': true,
      ['standing-playoff-match--' + round]: true,
      'play-off-match--past': match.result[0].is_played === 1,
      'play-off-match--future': match.result[0].is_played !== 1
    });
    const team1ClassNames = classNames({
      'play-off-match__side play-off-match-side play-off-match-side--left': true,
      'play-off-match-side--with-team': match.logo1 !== ''
    });
    const team2ClassNames = classNames({
      'play-off-match__side play-off-match-side play-off-match-side--right': true,
      'play-off-match-side--with-team': match.logo2 !== ''
    });
    return (
      <PlayOffMatchLink tag="div" match={match} className={matchClassNames}>
        <div className={team1ClassNames}>
          <Flag nameFull={match.team[0].team1} classes="play-off-match-side__flag" />
          <div className="play-off-match-side__country">{getCountryCode(match.team[0].team1)}</div>
        </div>
        <div className="play-off-match__score">
          {this.getScore(match)}
        </div>
        <div className={team2ClassNames}>
          <Flag nameFull={match.team[0].team2} classes="play-off-match-side__flag" />
          <div className="play-off-match-side__country">{getCountryCode(match.team[0].team2)}</div>
        </div>
      </PlayOffMatchLink>
    );
  }
}
