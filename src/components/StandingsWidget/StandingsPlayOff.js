import React, { Component, PropTypes } from 'react';
import StandingsPlayOffMatch from './StandingsPlayOffMatch';
import { map } from 'lodash';
import { KLink } from 'components';

export default class extends Component {
  static propTypes = {
    playoff: PropTypes.array.isRequired
  };
  convertName(round) {
    const names = {
      '1/4 финала': 14,
      '1/2 финала': 12,
      'Финал': 1,
      'За 3-е место': 3
    };
    return names[round];
  }
  render() {
    const { playoff } = this.props;
    return (
      <div className="standing-playoff">
        <KLink to="turnirnaya-tablica" className="standing-playoff__header top-slider__header">
          <div className="standing-playoff__header-left">ТУРНИРНАЯ ТАБЛИЦА</div>
        </KLink>
        <div className="standing-playoff__content">
          <div className="standing-playoff__labels">
            <div className="standing-playoff__label">
              <div className="standing-playoff__label-text">1/4 ФИНАЛА</div>
            </div>
            <div className="standing-playoff__label">
              <div className="standing-playoff__label-text">1/2 ФИНАЛА</div>
            </div>
            <div className="standing-playoff__label">
              <div className="standing-playoff__label-text">ФИНАЛ</div>
              <div className="standing-playoff__label-text standing-playoff__label-text--bottom">МАТЧ ЗА 3 МЕСТО</div>
            </div>
            <div className="standing-playoff__label">
              <div className="standing-playoff__label-text">1/2 ФИНАЛА</div>
            </div>
            <div className="standing-playoff__label">
              <div className="standing-playoff__label-text">1/4 ФИНАЛА</div>
            </div>
          </div>
          <div className="standing-playoff__matches">
            {map(playoff, (matches) =>
              map(matches.data, (match) =>
                <StandingsPlayOffMatch match={match} round={this.convertName(matches.name)} />
              )
            )}
          </div>
        </div>
      </div>
    );
  }
}
