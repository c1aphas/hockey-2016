import React, { Component, PropTypes } from 'react';
import StandingsGroup from './StandingsGroup';
import { map } from 'lodash';
import classNames from 'classnames';
import Scroll from 'helpers/Scroll';

import { connect } from 'react-redux';
import { standingsWidgetInit } from 'redux/modules/standingsWidget';

@connect(
  state => ({
    expanded: state.standingsWidget.expanded
  }), {...{standingsWidgetInit: standingsWidgetInit}}
)

export default class extends Component {
  static propTypes = {
    groups: PropTypes.array,
    expanded: PropTypes.bool,
    getCarousel: PropTypes.func,
    standingsWidgetInit: PropTypes.func
  };
  componentDidUpdate() {
    Scroll.scrollRelease();
    if (this.props.getCarousel()) {
      this.props.getCarousel().updateAutoHeight();
    }
  }
  doExpand(e) {
    e.preventDefault();
    Scroll.scrollLock();
    this.props.standingsWidgetInit(true);
  }
  render() {
    const { groups } = this.props;
    const moreClassNames = classNames({
      'standings__more': true,
      'hidden': this.props.expanded === true
    });
    return (
      <div className="standings__content">
        {map(groups, (group, idx) =>
          <StandingsGroup group={group} key={idx}/>
        )}
        <div className={moreClassNames} onClick={this.doExpand.bind(this)}>РАЗВЕРНУТЬ</div>
      </div>
    );
  }
}
