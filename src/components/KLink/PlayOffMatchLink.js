import KLink, {returnNull} from './KLink';
import {getCountrySlug} from 'helpers/Countries';
import moment from 'moment';

export default class PlayOffMatchLink extends KLink {
  static displayName = 'PlayOffMatchLink';
  static contextTypes = {
    ...KLink.contextTypes // HI IE!!!
  };
  static defaultProps = {
    ...KLink.defaultProps,
    match: null,
    to: 'raspisanie/'
  };
  render() {
    const match = this.props.match;
    if (match === null) {
      return returnNull.call(this);
    }
    const hasTeams = match.team[0] && match.team[0].team1_id > 0 && match.team[0].team1_id > 0;

    if (hasTeams === false) {
      return returnNull.call(this);
    }

    const date = moment(match.date[0], 'DD-MM-YYYY').locale('en').format('D[-]MMMM').toLowerCase();
    const team1 = getCountrySlug(match.team[0].team1) || match.team[0].team1;
    const team2 = getCountrySlug(match.team[0].team2) || match.team[0].team2;
    const to = `raspisanie/${match.result[0].match_id}/${date}/${team1}-${team2}/`; // raspisanie/448843/1-may/russia-norway'

    this.props = {
      ...this.props,
      to: to
    };
    return super.render();
  }
}
