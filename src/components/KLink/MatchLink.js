import KLink, {returnNull} from './KLink';
import {getCountrySlug} from 'helpers/Countries';
import moment from 'moment';

export default class MatchLink extends KLink {
  static displayName = 'MatchLink';
  static contextTypes = {
    ...KLink.contextTypes // HI IE!!!
  };
  static defaultProps = {
    ...KLink.defaultProps,
    match: null,
    to: 'raspisanie/'
  };
  render() {
    const match = this.props.match;
    if (match === null) {
      return returnNull.call(this);
    }
    const hasTeams = match.teams && match.teams[0].id > 0 && match.teams[1].id > 0;
    if (hasTeams === false) {
      return returnNull.call(this);
    }

    const date = moment.unix(match.pub_date).locale('en').format('D[-]MMMM').toLowerCase();
    const team1 = getCountrySlug(match.teams[0].name) || match.teams[0].name;
    const team2 = getCountrySlug(match.teams[1].name) || match.teams[1].name;
    const to = `raspisanie/${match.id}/${date}/${team1}-${team2}/`; // raspisanie/448843/1-may/russia-norway'

    this.props = {
      ...this.props,
      to: to
    };

    return super.render();
  }
}
