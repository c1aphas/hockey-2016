import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import config from 'config';
import { join as urlJoin } from 'path'; // warn: will work incorrect on Windows
import * as Nprogress from 'nprogress';
import isMobile from 'helpers/isMobile';

export function prefixUrl(url) {
  return urlJoin(config.app.rootPath, url);
}

export function returnNull() {
  if (this.props.tag) {
    return <this.props.tag {...this.props} style={{cursor: 'default'}}/>;
  }
  return (this.props.children);
}

@connect(
  state => ({
    isEmbed: state.app.isEmbed
  })
)
export default class KLink extends Link {
  static displayName = 'KLink';
  static contextTypes = {
    ...Link.contextTypes // HI IE!!!
  };
  static defaultProps = {
    ...Link.defaultProps,
    tag: ''
  };
  checkFirefox() {
    return global.navigator && global.navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
  }
  checkIE() {
    return global.navigator && global.navigator.userAgent.toLowerCase().indexOf('msie') > -1;
  }

  render() {
    const oldClickHandler = this.props.onClick;
    const tag = this.props.tag;
    let url = prefixUrl(this.props.to);
    if (this.props.isEmbed) {
      url = isMobile()
        ? 'http://sport.rambler.ru' + url + '?utm_source=mhead&utm_content=wch&utm_medium=feature&utm_campaign=self_promo'
        : 'http://sport.rambler.ru' + url + '?utm_source=head&utm_content=wch&utm_medium=feature&utm_campaign=self_promo';
    }
    this.props = {
      ...this.props,
      target: this.props.isEmbed ? '_blank' : null,
      onClick: event => {
        if (window && !this.props.isEmbed && (this.checkFirefox() || this.checkIE())) {
          event.preventDefault();
          window.location = this.props.to;
          return true;
        }

        if (global.location.pathname !== this.props.to && !this.props.isEmbed) {
          Nprogress.start();
        }
        if (oldClickHandler) {
          oldClickHandler(event);
        }
      },
      to: url,
      tag: tag
    };

    const result = super.render();

    if (tag) {
      return <this.props.tag {...this.props} onClick={this.handleClick} />;
    }

    return result;
  }
}
