import React, { Component } from 'react';
import broadcasts from '../../../static/data/video-broadcast.json';
import { Flag, Icon } from 'components';
import moment from 'moment';

export default class VideoBroadcast extends Component {
  constructor(props) {
    super(props);
    this.state = {play: false};
  }

  live() {
    const play = this.state.play;
    const states = {
      button:
        <div className="sketch sketch--video live-video live-video--online" onClick={this.togglePlay.bind(this)}>
          <div className="live-video__label">Идет трансляция</div>
          <div className="sketch__icon-play">
            <Icon name="video-full"/>
          </div>
        </div>,
      playing:
        <div className="sketch sketch--video live-video">
          <iframe width="100%" height="100%" src="https://stream.1tv.ru/embed" frameBorder="0" allowFullScreen></iframe>
        </div>
    };
    return play ? states.playing : states.button;
  }

  next(broadcast) {
    return (
      <div className="sketch sketch--video live-video">
        <div className="live-video__label">Ближайшая трансляция</div>
        <div className="live-video__info">
          <div className="live-video__team">
            {broadcast.team1}<Flag name={broadcast.team1} size="large" />
          </div>
          <div className="live-video__team-dash">&mdash;</div>
          <div className="live-video__team">
            <Flag name={broadcast.team2} size="large" />{broadcast.team2}
          </div>
        </div>
        <div className="live-video__time">{moment(new Date(broadcast.start)).format('D MMMM, H:mm')}</div>
      </div>
    );
  }

  drain() {
    return null;
  }

  togglePlay() {
    this.setState({play: !this.state.play});
  }

  render() {
    const now = Date.now();

    let live;
    let next;
    for (let i = 0; i < broadcasts.length; i++) {
      const cursor = broadcasts[i];

      if (cursor.start <= now && cursor.end > now) {
        live = cursor;
        break;
      }

      if (cursor.start > now) {
        next = cursor;
        break;
      }
    }

    if (live) {
      return this.live(live);
    }

    if (next) {
      return this.next(next);
    }

    return this.drain();
  }
}
