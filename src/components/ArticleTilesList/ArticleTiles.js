import React, {Component, PropTypes} from 'react';
import * as con from 'helpers/Constants';
import * as moment from 'moment';
import { KLink } from 'components';

export default class ArticleTiles extends Component {
  static propTypes = {
    article: PropTypes.object
  };

  render() {
    const article = this.props.article;
    const { image, internal_link: directLink, title, head, pub_date: pubDate, isOnline } = article;
    // TODO: Поставить реальную проверку на онлайн для события
    const online = isOnline ? <span className="sketch__live">онлайн</span> : '';

    return (
      <article className="sketch sketch_top">
        <KLink to={`/articles/${directLink}`} className="sketch__link">
          <div className="sketch__animate">
            <img src={`${con.URL_IMG_CHAMPIONAT}/${image.url}`} alt={image.title} className="sketch__img" />
            <div className="sketch__description">
              <div className="sketch__date">{moment.unix(pubDate).format('dddd, HH:mm')}{online}</div>
              <div className="sketch__title">{title}</div>
            </div>
          </div>
          <div className="sketch__text">{head}</div>
        </KLink>
      </article>
    );
  }
}
