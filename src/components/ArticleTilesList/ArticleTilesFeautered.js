import React, {Component, PropTypes} from 'react';
import * as con from 'helpers/Constants';
import { KLink } from 'components';

// todo: is this unused?

export default class ArticleTilesFeautered extends Component {
  static propTypes = {
    article: PropTypes.object
  };

  render() {
    const article = this.props.article;
    const { image, internal_link: directLink, title, isOnline } = article;
    const style = { backgroundImage: 'url(' + con.URL_IMG_CHAMPIONAT + '/' + image.url + ')' };
    // TODO: Поставить реальную проверку на онлайн для события
    const online = isOnline ? <div className="sketch__live-head">Онлайн</div> : '';

    return (
      <KLink to={`/articles/${directLink}`} className="sketch__link" style={style}>
        <img src={`${con.URL_IMG_CHAMPIONAT}/${image.url}`} alt={image.title} className="sketch__img sketch__img_head" style={{visibility: 'hidden'}}/>
        {online}
        <div className="sketch__title sketch__title_head">{title}</div>
      </KLink>
    );
  }
}
