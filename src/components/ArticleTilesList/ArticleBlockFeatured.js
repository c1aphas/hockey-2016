import React, {Component, PropTypes} from 'react';
import { ArticleTilesFeatured } from 'components';

export default class ArticleBlockFeatured extends Component {
  static propTypes = {
    articles: PropTypes.object
  };

  render() {
    const articles = this.props.articles;

    return (
      <article className="sketch sketch--feature sketch--online">
          <ArticleTilesFeatured article={articles} />
      </article>
    );
  }
}
