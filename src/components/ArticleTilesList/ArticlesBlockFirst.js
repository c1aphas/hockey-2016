import React, {Component, PropTypes} from 'react';
import { ArticleTiles } from 'components';
import map from 'lodash/map';

export default class ArticlesBlockFirst extends Component {
  static propTypes = {
    articles: PropTypes.array
  };

  render() {
    const articles = this.props.articles;

    return (
      <section className="column-large-29 column-medium-half column-small-half column-xs-half justify-flex">
        {map(articles, (article, key) =>
          <ArticleTiles article={article} key={key} />
        )}
      </section>
    );
  }
}
