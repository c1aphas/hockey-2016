import React, {Component, PropTypes} from 'react';
import { ArticleTiles, Ad } from 'components';
import classNames from 'classnames';

export default class ArticlesBlockSecond extends Component {
  static propTypes = {
    articles: PropTypes.array,
    loadMore: PropTypes.func,
    isLoading: PropTypes.bool,
    dispatch: PropTypes.func
  };

  render() {
    const load = this.props.loadMore;
    const articles = this.props.articles;
    const buttonClassNames = classNames({
      'button button--blue button--more': true,
      'button--loading': this.props.isLoading
    });

    const blocks = [];
    let k = 1;

    return (
      <section className="column-large-44 justify-flex">
        {articles.forEach(function renderArticles(article, key) {
          blocks[blocks.length] = <ArticleTiles article={article} key={key} />;
          const pos = key + 1;
          if ( pos % 12 === 0 && pos !== articles.length) {
            k++;
            blocks[blocks.length] = (<Ad
              id="context"
              mobileId="listing"
              scroll={false}
              index={k}
              mobileIndex={k}
              key={`ad${key}`}
            />);
          }
        })}

        {blocks}

        <div className="more">
          <div className={buttonClassNames} onClick={load}>Еще статьи
            <div className="loading-dots">
              <div className="loading-dot"></div>
              <div className="loading-dot"></div>
              <div className="loading-dot"></div>
            </div>
          </div>
        </div>
        <Ad id="context" mobileId="listing" type="footer" />
      </section>
    );
  }
}
