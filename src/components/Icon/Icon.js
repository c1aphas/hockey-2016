import React, { Component, PropTypes } from 'react';

export default class extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    class: PropTypes.string
  };
  render() {
    const useTag = `<use xlink:href="#${this.props.name}"></use>`;
    return (
      <i className={`icon icon-${this.props.name} ${this.props.class}`}>
        <svg className={`icon__svg icon-${this.props.name}`} dangerouslySetInnerHTML={{__html: useTag}}></svg>
      </i>
    );
  }
}
