import React, { Component, PropTypes } from 'react';
import { Countdown } from 'components';
import AnnouncementChampionat from './AnnouncementChampionat';

export default class extends Component {
  static propTypes = {
    timeleft: PropTypes.object
  };

  render() {
    return (
      <header className="header__announcement header-announcement">
        <div className="header-announcement__inner">
          <div className="header-announcement__event">До чемпионата осталось</div>
          <div className="header-announcement__timer header-announcement__timer--counter">
            <Countdown timeleft={this.props.timeleft} id="opening" />
          </div>
          <AnnouncementChampionat class="header-announcement__partner--counter" />
        </div>
      </header>
    );
  }
}
