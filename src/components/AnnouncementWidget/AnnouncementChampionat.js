import React, { Component, PropTypes } from 'react';
import { Icon } from 'components';
import * as constants from 'helpers/Constants';

export default class extends Component {
  static propTypes = {
    class: PropTypes.string
  };
  render() {
    return (
      <a href={constants.URL_WWW_CHAMPIONAT} target="_blank" className={`header-announcement__partner ${this.props.class}`}>Данные предоставил <Icon name="championat" class="header-announcement__championat" /> Чемпионат</a>
    );
  }
}
