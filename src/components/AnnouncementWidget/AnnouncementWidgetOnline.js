import React, { Component, PropTypes } from 'react';
import AnnouncementChampionat from './AnnouncementChampionat';
import { Flag, MatchLink } from 'components';
import { getCountryCode } from 'helpers/Countries';

export default class extends Component {
  static propTypes = {
    match: PropTypes.object
  };

  render() {
    const data = {
      time: this.props.match.time,
      status: this.props.match.status,
      score: [
        this.props.match.result.detailed.goal1,
        this.props.match.result.detailed.goal2
      ],
      teams: [
        {
          code: getCountryCode(this.props.match.teams[0].name)
        }, {
          code: getCountryCode(this.props.match.teams[1].name)
        }
      ]
    };

    return (
      <div className="header__announcement header-announcement">
        <div className="header-announcement__inner">
          <div className="header-announcement__scoreboard">
            <div className="header-announcement__team">
              <div className="header-announcement__side header-announcement__side-1">{data.teams[0].code}</div>
              <div className="flag header-announcement__flag">
                <Flag size="medium" name={data.teams[0].code} />
              </div>
            </div>
            <div className="header-announcement__score">{data.score[0]}:{data.score[1]}</div>
            <div className="header-announcement__team">
              <div className="flag header-announcement__flag">
                <Flag size="medium" name={data.teams[1].code} />
              </div>
              <div className="header-announcement__side header-announcement__side-2">{data.teams[1].code}</div>
            </div>
          </div>
          <div className="header-announcement__game">
            <span className="header-announcement__period">{data.status}</span>
            <span className="header-announcement__time">{data.time}</span>
          </div>
          <MatchLink match={this.props.match} className="header-announcement__live">Текстовая трансляция</MatchLink>
          <AnnouncementChampionat />
        </div>
      </div>
    );
  }
}
