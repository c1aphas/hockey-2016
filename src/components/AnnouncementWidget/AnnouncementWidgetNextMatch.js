import React, { Component, PropTypes } from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { getCountryCode } from 'helpers/Countries';
import { Flag, Countdown, KLink } from 'components';
import AnnouncementChampionat from './AnnouncementChampionat';

@connect(
  state => ({
    isEmbed: state.app.isEmbed
  })
)
export default class extends Component {
  static propTypes = {
    match: PropTypes.object,
    timeleft: PropTypes.object,
    isEmbed: PropTypes.bool
  };

  MatchInfo(props) {
    const { isEmbed, children } = props;
    return isEmbed
      ? <KLink to="/">{children}</KLink>
      : <div>{children}</div>;
  }

  render() {
    const team1Code = getCountryCode(this.props.match.teams[0].name);
    const team2Code = getCountryCode(this.props.match.teams[1].name);
    const countdown = this.props.timeleft.asSeconds() <= 0
      ? 'Матч скоро начнется'
      : <Countdown timeleft={this.props.timeleft} id={`nextmatch-${this.props.match.id}`} addon="До матча" />;

    return (
      <div className="header__announcement header-announcement">
        <div className="header-announcement__inner">
          <this.MatchInfo {...this.props}>
            <div className="header-announcement__scoreboard">
              <div className="header-announcement__team">
                <div className="header-announcement__side header-announcement__side-1">{team1Code}</div>
                <div className="flag header-announcement__flag">
                  <Flag size="medium" name={team1Code} />
                </div>
              </div>
              <div className="header-announcement__score">–:–</div>
              <div className="header-announcement__team">
                <div className="flag header-announcement__flag">
                  <Flag size="medium" name={team2Code} />
                </div>
                <div className="header-announcement__side header-announcement__side-2">{team2Code}</div>
              </div>
            </div>
            <div className="header-announcement__game">
              <span className="header-announcement__time">{
                moment(this.props.match.date + ' ' + this.props.match.time).format('dddd HH:mm')
              }</span>
            </div>
            <div className="header-announcement__timer header-announcement__timer--gray">
              {countdown}
            </div>
          </this.MatchInfo>
          <AnnouncementChampionat />
        </div>
      </div>
    );
  }
}
