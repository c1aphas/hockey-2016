import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Icon } from 'components';
import { map } from 'lodash';

export default class SocialButtons extends Component {
  static propTypes = {
    classes: PropTypes.string
  };

  componentDidMount() {
    // todo: is this correct way?
    if (typeof window === 'undefined') {
      return;
    }

    window.socialLikesButtons = {
      livejournal: {
        icon: '',
        popupUrl: 'http://www.livejournal.com/update.bml?subject={title}&event={url}',
        popupWidth: 650,
        popupHeight: 500
      }
    };

    const likes = require('social-likes-next').default;
    likes(ReactDOM.findDOMNode(this));
  }

  static services = {
    vk: 'vkontakte',
    fb: 'facebook',
    ok: 'odnoklassniki',
    lj: 'livejournal',
    tw: 'twitter'
  };

  button(type, key) {
    return (
      <div className={`social-likes__item social-likes__item--${key}`} key={`social-button-${key}`} data-service={type}>
        <Icon name={`social-${key}`} class="social-likes__icon" />
      </div>
    );
  }

  render() {
    return (
      <div className={`social-likes ${this.props.classes || ''}`}>
        {map(SocialButtons.services, this.button)}
      </div>
    );
  }
}
