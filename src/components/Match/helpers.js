import React from 'react';

export function shortName(name) {
  const parts = name.split(' ');
  const firstName = parts.shift()[0];
  const lastName = parts.join(' ');
  return <span>{firstName}.&nbsp;{lastName}</span>;
}
