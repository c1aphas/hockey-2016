import React, {Component, PropTypes} from 'react';

// warn: this is quick-n-dirty tabs implementation and it should be replaced with vendor component
// in case it required anywhere else, or we just have enough time to find the good one
export default class extends Component {
  static propTypes = {
    match: PropTypes.object
  };

  componentDidMount() {
    this.tabHeaderClick({
      preventDefault: () => {},
      target: document.querySelector('.match-protocol-tabs__item')
    });
  }

  tabHeaderClick(event) {
    event.preventDefault();

    const activeClass = 'match-protocol-tabs__item--active';
    const tabs = document.querySelector('.match-protocol-tabs');
    if (!tabs) {
      return;
    }
    [].forEach.call(tabs.childNodes, node =>
      node.classList.remove(activeClass)
    );

    if (event.target) {
      event.target.classList.add(activeClass);

      const targetTab = event.target.getAttribute('data-tab-panel');
      [].forEach.call(document.querySelectorAll('.protocol-tab'), node =>
        node.style.display = node.classList.contains(targetTab) ? 'block' : 'none'
      );
    }
  }

  link(panel, text) {
    return <a href="#" className="match-protocol-tabs__item" data-tab-panel={panel} onClick={this.tabHeaderClick}>{text}</a>;
  }

  render() {
    const stats = this.props.match.stat || {};

    const hasStats = !!(stats.stat && stats.stat.length);
    const hasSquads = stats.events && stats.events.lineup;

    if (!hasStats && !hasSquads) {
      return null;
    }

    return (
      <div className="match-protocol-tabs">
        {hasStats && this.link('match-protocol-stats-panel', 'Статистика матча')}
        {hasSquads && this.link('match-protocol-squads-panel', 'Состав команд')}
      </div>
    );
  }
}
