import React from 'react';
import { groupBy, find } from 'lodash';
import { shortName } from './helpers';

function squadMember(guy) {
  return (
    <div className="match-protocol-squads__player" key={`squad-member-${guy.player_id}`}>
      <span className="match-protocol-squads__number">{guy.number}</span>
      <span className="match-protocol-squads__name">{shortName(guy.player_name)}</span>
    </div>
  );
}

function normalizeCoachName(fullName) {
  const segments = fullName.split(' ');
  return segments.shift() + ' ' + segments.pop();
}

function outerWrap(content) {
  return (
    <div className="protocol-tab match-protocol-squads-panel">
      <div className="match-protocol-squads">
        {content}
      </div>
    </div>
  );
}

export default function(match) {
  if (!match.stat.events || !match.stat.events.lineup) {
    return outerWrap('');
  }

  const lineup = match.stat.events.lineup;
  const perTeam = groupBy(lineup, guy => guy.team);

  const left = groupBy(perTeam[1], guy => guy.amplua);
  const right = groupBy(perTeam[2], guy => guy.amplua);

  const coachNames = match.stat.events.coach.map(coach => ({
    name: normalizeCoachName(coach.name),
    team: coach.team
  }));

  const content = (
    <div className="protocol-tab match-protocol-squads-panel">
      <div className="match-protocol-squads">
        <div className="match-protocol-squads__item match-protocol-squads__item--title-left">
          Защитники
        </div>

        <div className="match-protocol-squads__item match-protocol-squads__item--title-center">
          Нападающие
        </div>

        <div className="match-protocol-squads__item match-protocol-squads__item--title-right">
          Защитники
        </div>

        <div className="match-protocol-squads__item match-protocol-squads__item--defender-left">
          {left['зщ'].map(squadMember)}
        </div>

        <div className="match-protocol-squads__item match-protocol-squads__item--forward-left">
          {left['нп'].map(squadMember)}
        </div>

        <div className="match-protocol-squads__item match-protocol-squads__item--forward-right">
          {right['нп'].map(squadMember)}
        </div>

        <div className="match-protocol-squads__item match-protocol-squads__item--defender-right">
          {right['зщ'].map(squadMember)}
        </div>

        <div className="match-protocol-squads__item match-protocol-squads__item--title-l-bottom">
          Вратари
        </div>

        <div className="match-protocol-squads__item match-protocol-squads__item--title-l-center">
          Тренер
        </div>

        <div className="match-protocol-squads__item match-protocol-squads__item--title-r-center">
          Тренер
        </div>

        <div className="match-protocol-squads__item match-protocol-squads__item--title-r-bottom">
          Вратари
        </div>

        <div className="match-protocol-squads__item match-protocol-squads__item--keeper-left">
          {left['вр'].map(squadMember)}
        </div>

        <div className="match-protocol-squads__item match-protocol-squads__item--coach-left">
          <span className="match-protocol-squads__name">
            {shortName(find(coachNames, coach => coach.team === 1).name)}
          </span>
        </div>

        <div className="match-protocol-squads__item match-protocol-squads__item--coach-right">
          <span className="match-protocol-squads__name">
            {shortName(find(coachNames, coach => coach.team === 2).name)}
          </span>
        </div>

        <div className="match-protocol-squads__item match-protocol-squads__item--keeper-right">
          {right['вр'].map(squadMember)}
        </div>
      </div>
    </div>
  );

  return outerWrap(content);
}
