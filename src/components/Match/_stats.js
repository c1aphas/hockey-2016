import React from 'react';
import keyBy from 'lodash/keyBy';

function trimBraces(input) {
  return input.replace(/[()]/g, '');
}

function statsRow(displayName, stats) {
  if (!stats) {
    return '';
  }

  return (
    <div className="match-protocol-stats__row">
      <div className="match-protocol-stats__num">
        <div className="match-protocol-stats__item match-protocol-stats__item--left">
          <span className="match-protocol-stats__total">
            {stats.team1}
          </span>
          <span className="match-protocol-stats__periods">
            {trimBraces(stats.team1_note)}
          </span>
        </div>

        <div className="match-protocol-stats__head">
          {displayName}
        </div>

        <div className="match-protocol-stats__item match-protocol-stats__item--right">
          <span className="match-protocol-stats__periods">
            {trimBraces(stats.team2_note)}
          </span>
          <span className="match-protocol-stats__total">
            {stats.team2}
          </span>
        </div>
      </div>

      <div className="match-protocol-stats__line">
        <div className="match-protocol-stats__part match-protocol-stats__part--left" style={{ width: stats.p1 + '%' }}></div>
        <div className="match-protocol-stats__part match-protocol-stats__part--right" style={{ width: stats.p2 + '%' }}></div>
      </div>
    </div>
  );
}

export default function(match) {
  const stats = keyBy(match.stat.stat, 'type');
  const classList = stats ? 'protocol-tab match-protocol-stats-panel' : 'protocol-tab match-protocol-stats-panel match-protocol-stats-panel--hidden';

  return (
    <div className={classList}>
      <div className="match-protocol-stats">
        {statsRow('Броски', stats.shotin)}
        {statsRow('Вбрасывания', stats.faceoff)}
        {statsRow('Штрафы', stats.pentime)}
      </div>
    </div>
  );
}
