import React from 'react';
import { shortName } from './helpers';
import { getCountryCode } from 'helpers/Countries';
import { Flag } from 'components';

function listTeamGoals(match, teamIdx) {
  if (!match.stat.events || !match.stat.events.hgoal) {
    return [];
  }

  return match.stat.events.hgoal.filter(goal => goal.team === teamIdx);
}

function goalAuthors(goal, isReverse) {
  return (
    <div className={`match-protocol-summary__chronology-players ${isReverse ? 'match-protocol-summary__chronology-players--reverse' : ''}`}>
      <div className="match-protocol-summary__chronology-scored">
        {shortName(goal.player_name)}
      </div>
      {(goal.pass || []).map(pass => (
        <div className="match-protocol-summary__chronology-assist" key={`goal-pass-${pass.index}`}>
          {shortName(pass.player_name)}
        </div>
      ))}
    </div>
  );
}

function goalMinute(goal) {
  return (
    <div className="match-protocol-summary__chronology-minute">
      {goal.minute}
    </div>
  );
}

function goalType(goal) {
  return (
    <div className="match-protocol-summary__chronology-type">
      {goal.desc}
    </div>
  );
}

function country(name) {
  return (
    <div className="match-protocol-summary__scoreboard-country">
      <span className="match-protocol-summary__scoreboard-country-full">{name}</span>
      <span className="match-protocol-summary__scoreboard-country-short">{getCountryCode(name)}</span>
    </div>
  );
}

function largeFlag(team) {
  return <Flag size="large" name={getCountryCode(team.name)} classes="match-protocol-summary__scoreboard-flag" />;
}

export default function(match) {
  return (
    <div className="match-protocol-summary">
      <div className="match-protocol-summary__info">
        <div className="match-protocol-summary__stadium">
          <div className="match-protocol-summary__stadium-name">
            {match.stadium.name}
          </div>
          <div className="match-protocol-summary__stadium-city">
            {match.stadium.city}
          </div>
        </div>

        <div className="match-protocol-summary__group">
          {match.round}
        </div>

        <div className="match-protocol-summary__date">
          {match.date}
        </div>

        <div className="match-protocol-summary__time">
          {match.time}
        </div>
      </div>

      <div className="match-protocol-summary__status">
        {match.status} {match.status_extra}
      </div>

      <div className="match-protocol-summary__scoreboard">
        <div className="match-protocol-summary__scoreboard-team match-protocol-summary__scoreboard-team--left">
          {country(match.teams[0].name)}
          {largeFlag(match.teams[0])}
        </div>

        <div className="match-protocol-summary__scoreboard-score">{match.result.main ? match.result.main : '–:–' }</div>

        <div className="match-protocol-summary__scoreboard-team match-protocol-summary__scoreboard-team--right">
          {largeFlag(match.teams[1])}
          {country(match.teams[1].name)}
        </div>
      </div>

      <div className="match-protocol-summary__chronology">
        <div className="match-protocol-summary__chronology-team match-protocol-summary__chronology-team--left">
          {listTeamGoals(match, 1).map(goal =>
            <div className="match-protocol-summary__chronology-row" key={`goal-row-${goal.index}`}>
              {goalAuthors(goal)}
              {goalMinute(goal)}
              {goalType(goal)}
            </div>
          )}
        </div>

        <div className="match-protocol-summary__chronology-score">
          {match.result.full_res.split(' ').map((entry, idx) =>
            <span className="match-protocol-summary__chronology-part" key={`score-part-${idx}`}>
              {entry}
            </span>
          )}

          <div className="match-protocol-summary__chronology-referees">
            {match.referees.map(referee =>
              <div key={`referee-${referee.id}`}>
                <div className="match-protocol-summary__chronology-title">
                  {referee.type_title}
                </div>
                <div className="match-protocol-summary__chronology-name">
                  {shortName(referee.name)}, {referee.city.replace(/[()]/g, '')}
                </div>
              </div>
            )}
          </div>
        </div>

        <div className="match-protocol-summary__chronology-team match-protocol-summary__chronology-team--right">
          {listTeamGoals(match, 2).map(goal =>
            <div className="match-protocol-summary__chronology-row match-protocol-summary__chronology-row--reverse" key={`goal-row-${goal.index}`}>
              {goalAuthors(goal, true)}
              {goalMinute(goal)}
              {goalType(goal)}
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
