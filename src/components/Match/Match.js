import React, { Component, PropTypes } from 'react';
import TabsToggler from './_tabsToggler';
import summary from './_summary';
import stats from './_stats';
import squads from './_squads';
import broadcast from './_broadcast';
import { SocialButtons } from 'components';

export default class extends Component {
  static propTypes = {
    match: PropTypes.object,
    children: PropTypes.object
  };

  render() {
    const match = this.props.match.data;

    return (
      <div className="match-protocol block block--white">
        {this.props.children}

        {summary(match)}

        <TabsToggler match={match} />

        {stats(match)}

        {squads(match)}

        { /* todo: video here */ }

        {broadcast(match)}

        <SocialButtons classes="match-protocol__social" />
      </div>
    );
  }
}
