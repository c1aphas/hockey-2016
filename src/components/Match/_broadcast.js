import React from 'react';
import { Icon } from 'components';

const eventClasses = {
  hgoal: 'match-protocol-online__row--goal',
  hpen: 'match-protocol-online__row--penalty'
};

function getEventClasses(event) {
  return eventClasses[event.type] || '';
}

const alerts = {
  hgoal: event => (
    <div className="score match-protocol-online__score">
      {event.result}
    </div>
  ),
  hpen: () => (
    <Icon name="attention" class="match-protocol-online__icon-attention" />
  )
};

function getEventAlert(event) {
  const handler = alerts[event.type];
  return handler ? handler(event) : '';
}

export default function(match) {
  if (!match.text || !match.text.length) {
    return '';
  }

  const text = match.text;

  return (
    <div className="match-protocol-online block block--white">
      <h2 className="block__header match-protocol-online__header">
        Текстовая трансляция
      </h2>

      <table className="match-protocol-online__table">
        <tbody>
        {text.map(event =>
          <tr className={`match-protocol-online__row ${getEventClasses(event)}`} key={`broadcast-msg-${event.id}`}>
            <td className="match-protocol-online__minute">
              {event.minute}
            </td>

            <td className="match-protocol-online__alert">
              {getEventAlert(event)}
            </td>

            <td className="match-protocol-online__text" dangerouslySetInnerHTML={{ __html: event.text }}></td>
          </tr>
        )}
        </tbody>
      </table>
    </div>
  );
}
