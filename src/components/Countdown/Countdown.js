import React, { Component, PropTypes } from 'react';
import moment from 'moment';
import plural from 'helpers/plural';
import { renderToStaticMarkup } from 'react-dom/server';
import ReactDOM from 'react-dom';

export default class Countdown extends Component {
  static propTypes = {
    id: PropTypes.string.isRequired,
    timeleft: PropTypes.object.isRequired,
    addon: PropTypes.string
  };

  componentDidMount() {
    let addon = '';
    if (this.props.addon) {
      addon = renderToStaticMarkup(
        <span className="countdown__item">
          <span className="countdown__value">{this.props.addon}</span>
        </span>
      );
    }
    // insert counter markup as raw html, after component did mount
    // TODO support for multiple components on page?
    ReactDOM.render(
      <div dangerouslySetInnerHTML={{ __html: this.createMarkup() + addon }}></div>,
      document.querySelector('.countdown')
    );
  }

  createMarkup() {
    const duration = moment.duration(this.props.timeleft);
    const labels = [(part) => plural('день_дня_дней', part), 'Ч', 'МИН'];
    const markup = [duration.days(), duration.hours(), duration.minutes()].map((part, idx) => {
      if (part) {
        return renderToStaticMarkup(
          <span className="countdown__item">
            <span className="countdown__value">{part}</span>
            <span className="countdown__label">
              {typeof labels[idx] === 'function' ? labels[idx](part) : labels[idx]}
            </span>
          </span>
        );
      }
    }).join('');

    return markup
      ? markup
      : renderToStaticMarkup(
          <span className="countdown__item">
            <span className="countdown__label">Меньше минуты</span>
          </span>
        );
  }

  render() {
    return <div className="countdown"></div>;
  }
}
