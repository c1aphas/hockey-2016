import moment from 'moment';
const linkPattern = /(.+?)-(\d+)-(.+?)\.(.*)/;

export default function transformResponse(data) {
  data.forEach(item => {
    const matches = item.direct_link.split('/').pop().match(linkPattern) || [];
    item.internal_link = item._id + '-' + matches[3];

    item.group_key = moment.unix(item.pub_date).format('YYMMDD');
  });

  return Promise.resolve(data);
}
