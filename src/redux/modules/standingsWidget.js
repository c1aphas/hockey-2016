export const INIT = 'standingsWidget/INIT';

const initialState = {
  expanded: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case INIT:
      return {
        expanded: action.expanded
      };
    default:
      return state;
  }
}

export function standingsWidgetInit(expanded, counterId) {
  return {
    type: INIT,
    expanded: expanded,
    widgetId: counterId // MAY BE later
  };
}
