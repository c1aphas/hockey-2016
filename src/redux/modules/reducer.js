import { combineReducers } from 'redux';
import { routeReducer } from 'react-router-redux';
import { reducer as reduxAsyncConnect } from 'redux-async-connect';

import articles from './articles';
import articlesTiles from './articlesTiles';
import news from './news';
import standings from './standings';
import publication from './publication';
import schedule from './schedule';
import match from './match';
import standingsWidget from './standingsWidget';
import playerStats from './playerStats';
import app from './app';

export default combineReducers({
  routing: routeReducer,
  app,
  reduxAsyncConnect,
  standings,
  articles,
  articlesTiles,
  news,
  publication,
  schedule,
  match,
  standingsWidget,
  playerStats
});
