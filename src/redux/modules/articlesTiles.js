import transformResponse from 'redux/transformers/news';

export const LOAD = 'ArticleTiles/LOAD';
export const LOAD_SUCCESS = 'ArticleTiles/LOAD_SUCCESS';
export const LOAD_FAIL = 'ArticleTiles/LOAD_FAIL';

const initialState = {
  loaded: false,
  data: {
    articleFeature: null,
    articlesBlockFirst: null,
    articlesBlockSecond: [],
    articlesLastId: 0
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: {
          articleFeature: state.data.articleFeature || action.result[0],
          articlesBlockFirst: state.data.articlesBlockFirst || action.result.slice(1, 5),
          articlesBlockSecond: [
            ...state.data.articlesBlockSecond,
            ...(state.data.articlesBlockSecond.length ? action.result.slice(0, 6) : action.result.slice(5, 11))
          ],
          articlesLastId: 1
        },
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        items: null, // this will remove already fetched news. is this ok?
        error: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(state) {
  return state.articlesTiles && state.articlesTiles.loaded;
}

export function load(lastId) {
  const url = '/stream?section=whc&type=article&sport=hockey' + (lastId ? '&before=' + lastId : '');

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: client => client.get(url).then(transformResponse)
  };
}

// todo: this repeats load()
export function loadMore(lastId) {
  const url = '/stream?section=whc&type=article&sport=hockey' + (lastId ? '&before=' + lastId : '');

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: client => client.get(url).then(transformResponse)
  };
}

export function rollBack(dispatch, getState) {
  const state = getState();

  state.articlesTiles.data = {
    articleFeature: state.articlesTiles.data.articleFeature,
    articlesBlockFirst: state.articlesTiles.data.articlesBlockFirst.slice(0, 4),
    articlesBlockSecond: state.articlesTiles.data.articlesBlockSecond.slice(0, 6),
    articlesLastId: 1
  };
}
