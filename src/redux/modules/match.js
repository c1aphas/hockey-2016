export const LOAD = 'Match/LOAD';
export const LOAD_SUCCESS = 'Match/LOAD_SUCCESS';
export const LOAD_FAIL = 'Match/LOAD_FAIL';

const initialState = {
  items: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        items: {
          ...state.items,
          [action.matchId]: {
            loading: true
          }
        }
      };
    case LOAD_SUCCESS:
      return {
        items: {
          ...state.items,
          [action.matchId]: {
            loading: false,
            loaded: true,
            match: action.result,
            error: null
          }
        }
      };
    case LOAD_FAIL:
      return {
        items: {
          ...state.items,
          [action.matchId]: {
            loading: false,
            loaded: false,
            match: null,
            error: action.error
          }
        }
      };
    default:
      return state;
  }
}

export function isLoaded(state, id) {
  return state.match && state.match.items[id] && state.match.items[id].loaded;
}

export function load(id) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: client => client.get(`/stat/match/${id}?sport=hockey&need_stat=1&need_text=1&need_video=0`),
    matchId: id
  };
}
