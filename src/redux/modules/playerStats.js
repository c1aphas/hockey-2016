import config from 'config';

export const LOAD = 'PlayerStats/LOAD';
export const LOAD_SUCCESS = 'PlayerStats/LOAD_SUCCESS';
export const LOAD_FAIL = 'PlayerStats/LOAD_FAIL';

const initialState = {
  goalpass: {},
  assistent: {},
  keeper: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        [action.section]: {
          loading: true
        }
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        [action.section]: {
          loading: false,
          loaded: true,
          players: action.result.data && action.result.data.length ? action.result.data[0] : [],
          error: null
        }
      };
    case LOAD_FAIL:
      return {
        ...state,
        [action.section]: {
          loading: false,
          loaded: false,
          players: [],
          error: action.error
        }
      };
    default:
      return state;
  }
}

export function isLoaded(state, section) {
  return state.playerStats && state.playerStats[section] && state.playerStats[section].loaded;
}

export function load(section) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: client => client.get(`sports/hockey/tournaments/${config.app.tournamentId}/players/stat/${section}`),
    section
  };
}
