// todo: this module is probably not used

// ActionTypes
export const LOAD = 'Articles/LOAD';
export const LOAD_SUCCESS = 'Articles/LOAD_SUCCESS';
export const LOAD_FAIL = 'Articles/LOAD_FAIL';

const initialState = {
  loaded: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: {
          articleFeature: action.result[0],
          articlesBlockFirst: action.result.slice(1, 4),
          articlesBlockSecond: action.result.slice(5, 10)
        },
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.articles && globalState.articles.loaded;
}

export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/stream?section=whc&type=article&sport=hockey')
  };
}
