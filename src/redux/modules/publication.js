export const LOAD = 'Publication/LOAD';
export const LOAD_SUCCESS = 'Publication/LOAD_SUCCESS';
export const LOAD_FAIL = 'Publication/LOAD_FAIL';

const initialState = {
  items: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        items: {
          ...state.items,
          [action.pubId]: {
            loading: true
          }
        }
      };
    case LOAD_SUCCESS:
      return {
        items: {
          ...state.items,
          [action.pubId]: {
            loading: false,
            loaded: true,
            pub: action.result[0],
            error: null
          }
        }
      };
    case LOAD_FAIL:
      return {
        items: {
          ...state.items,
          [action.pubId]: {
            loading: false,
            loaded: false,
            pub: null,
            error: action.error
          }
        }
      };
    default:
      return state;
  }
}

export function isLoaded(state, id) {
  return state.publication && state.publication.items[id] && state.publication.items[id].loaded;
}

export function load(id, slug) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: client => client.get('/stream/' + id),
    pubId: id,
    pubSlug: slug
  };
}
