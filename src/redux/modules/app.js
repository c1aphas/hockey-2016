export const EMBED_MODE = 'App/EMBED_MODE';

const initialState = {
  isEmbed: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case EMBED_MODE:
      return {
        isEmbed: action.isEmbed
      };
    default:
      return state;
  }
}

export function toggleEmbedMode(isEmbed) {
  return {
    type: EMBED_MODE,
    isEmbed: isEmbed
  };
}
