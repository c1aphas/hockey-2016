import transformResponse from 'redux/transformers/news';

export const LOAD = 'News/LOAD';
export const LOAD_SUCCESS = 'News/LOAD_SUCCESS';
export const LOAD_FAIL = 'News/LOAD_FAIL';

const initialState = {
  loaded: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        items: [
          ...(state.items || []),
          ...action.result
        ],
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        items: null, // this will remove already fetched news. is this ok?
        error: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(state) {
  return state.news && state.news.loaded;
}

export function load(lastId) {
  const url = '/stream?sport=hockey&section=whc&type=news' + (lastId ? '&before=' + lastId : '');

  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: client => client.get(url).then(transformResponse)
  };
}

export function rollBack(dispatch, getState) {
  const state = getState();
  state.news.items = state.news.items.slice(0, 20);
}
