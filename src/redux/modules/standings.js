import { app as appConfig } from 'config';

// ActionTypes
export const LOAD = 'Standings/LOAD';
export const LOAD_SUCCESS = 'Standings/LOAD_SUCCESS';
export const LOAD_FAIL = 'Standings/LOAD_FAIL';

const initialState = {
  loaded: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result,
        error: null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        data: null,
        error: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.standings && globalState.standings.loaded;
}

export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/sports/hockey/tournaments/' + appConfig.tournamentId + '/table')
  };
}
