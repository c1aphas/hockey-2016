window.timeoutList = [];
window.intervalList = [];

window.oldSetTimeout = window.setTimeout;
window.oldSetInterval = window.setInterval;
window.oldClearTimeout = window.clearTimeout;
window.oldClearInterval = window.clearInterval;

window.setTimeout = (code, delay) => {
  const id = window.oldSetTimeout(code, delay);
  window.timeoutList.push(id);
  return id;
};
window.clearTimeout = (id) => {
  const ind = window.timeoutList.indexOf(id);
  if (ind >= 0) {
    window.timeoutList.splice(ind, 1);
  }
  return window.oldClearTimeout(id);
};
window.setInterval = (code, delay) => {
  const id = window.oldSetInterval(code, delay);
  window.intervalList.push(id);
  return id;
};
window.clearInterval = (id) => {
  const index = window.intervalList.indexOf(id);
  if (index >= 0) {
    window.intervalList.splice(index, 1);
  }
  return window.oldClearInterval(id);
};
window.clearAllTimeouts = () => {
  for (const i in window.timeoutList) {
    if (window.timeoutList.hasOwnProperty(i)) {
      window.oldClearTimeout(window.timeoutList[i]);
    }
  }
  window.timeoutList = [];
};
window.clearAllIntervals = () => {
  for (const i in window.intervalList) {
    if (window.intervalList.hasOwnProperty(i)) {
      window.oldClearInterval(window.intervalList[i]);
    }
  }
  window.intervalList = [];
};
