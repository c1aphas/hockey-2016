import isMobile from 'helpers/isMobile';
import _ from 'lodash';

export default class adfoxLoader {
  constructor(bannerName, params) {
    this.bannerName = bannerName;
    this.params = params || {};
    this.bannerVisible = false;
    this.mobile = isMobile();
    this.iterator = 1;
    this.init();
    this.render();
  }

  init() {
    const BANNERS_PARAMS = {
      'billboard': {
        adfoxParams: {
          p1: 'buwjh',
          p2: 'y',
          pct: 'a'
        }
      },
      '240x400': {
        adfoxParams: {
          p1: 'buwlf',
          p2: 'emhk',
          pct: 'a',
          criteo: 'crramb240=1'
        },
        onRender: () => {
          if (this.$placeholder.offsetHeight >= 500) {
            const placeholder240x200 = document.getElementById('adfox-240x200');
            if (placeholder240x200.firstChild) {
              placeholder240x200.innerHTML = '';
            }
          }
        }
      },
      '240x200': {
        adfoxParams: {
          p1: 'buwli',
          p2: 'ewwf',
          pct: 'a'
        }
      },
      'superfooter': {
        adfoxParams: {
          p1: 'buwjj',
          p2: 'fcuz',
          pct: 'a'
        },
        scroll: true,
        onRender: this.renderWithGap.bind(this)
      },
      'inread-1': {
        adfoxParams: {
          p1: 'buwjl',
          p2: 'fcvb',
          pct: 'a'
        },
        onRender: this.renderWithGap.bind(this)
      },
      'inread-2': {
        adfoxParams: {
          p1: 'buydt',
          p2: 'fhzx',
          pct: 'a'
        },
        onRender: this.renderWithGap.bind(this)
      },
      'context': {
        adfoxParams: {
          p1: 'buwjm',
          p2: 'ewzc',
          pct: 'a'
        },
        scroll: true,
        onRender: this.renderWithGap.bind(this)
      },
      // mobiles codes below
      'inpage': {
        adfoxParams: {
          p1: 'buwjp',
          p2: 'fexd',
          pct: 'a'
        },
        scroll: true
      },
      'topbanner': {
        adfoxParams: {
          p1: 'buwjn',
          p2: 'fexc',
          pct: 'a'
        },
        onRender: this.renderWithGap.bind(this)
      },
      'listing': {
        adfoxParams: {
          p1: 'buwjo',
          p2: 'emil',
          pct: 'b',
          criteo: 'crrambm300=1'
        },
        scroll: true,
        onRender: this.renderWithGap.bind(this)
      },
      'content': {
        adfoxParams: {
          p1: 'buwjo',
          p2: 'emil',
          pct: 'b',
          criteo: 'crrambm300=1'
        },
        scroll: true,
        onRender: this.renderWithGap.bind(this)
      }
    };

    if (this.bannerName === 'disabled') {
      return false;
    }

    this.params = _.defaultsDeep(this.params, BANNERS_PARAMS[this.bannerName]);
  }

  renderWithGap() {
    if (this.bannerVisible) {
      this.$placeholder.classList.add('ad--loaded');
    }
  }

  checkBannerLoaded() {
    // check ad loaded
    const bannerParams = this.params;
    let visibilityCheckComplete = false;
    const startTime = new Date();
    const bannerTimeOut = this.params.bannerTimeOut ? this.params.bannerTimeOut : 5000;
    const checkInterval = setInterval(() => {
      const currentTime = new Date();

      function checkComplete() {
        visibilityCheckComplete = true;
        clearInterval(checkInterval);
        if (bannerParams.onRender) {
          bannerParams.onRender();
        }
      }

      if (visibilityCheckComplete === false) {
        if (this.$placeholder.offsetHeight > 5) {
          this.bannerVisible = true;
          checkComplete();
        } else if (currentTime - startTime >= bannerTimeOut && !this.bannerVisible) {
          if (bannerParams.logErrors) {
            console.error('Banner [' + this.bannerName + '] load timed out');
          }
          checkComplete();
        }
      }
    }, 50);
  }

  render() {
    let adfMethod;
    this.$placeholder = document.getElementById(this.params.placeholderID);

    if (this.$placeholder === null) {
      return;
    }

    this.params.rtb = this.params.rtb === undefined ? false : this.params.rtb;
    this.params.scroll = this.params.scroll === undefined ? false : this.params.scroll;

    if (this.params.rtb && this.params.scroll) {
      adfMethod = 'showScroll_b';
    } else if (this.params.rich === true) {
      adfMethod = 'showRich';
    } else if (this.params.rtb && this.params.scroll === false) {
      adfMethod = 'show_b';
    } else if (this.params.rtb === false && this.params.scroll) {
      adfMethod = 'showScroll';
    } else if (this.params.rtb === false && this.params.scroll === false) {
      adfMethod = 'show';
    }

    // try to load
    if (global.Adf && global.Adf.banner) {
      global.Adf.banner[adfMethod](
        this.params.placeholderID,
        this.params.adfoxParams
      );
    }

    if (this.params.scroll) {
      window.addEventListener('scroll', () => {
        if (this.$placeholder.getBoundingClientRect().top - window.innerHeight <= 0 &&
            !this.$placeholder.classList.contains('ad--loaded')
        ) {
          this.checkBannerLoaded();
        }
      });
    } else {
      this.checkBannerLoaded();
    }
  }

  redraw() {
    const $placeholder = document.getElementById(this.params.placeholderID);
    if (!global.Adf || !$placeholder || this.params.scroll) {
      return;
    }
    global.Adf.banner.reload(this.params.placeholderID, this.params.adfoxParams);
    this.checkBannerLoaded();
  }
}
