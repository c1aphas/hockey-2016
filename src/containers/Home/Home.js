import React, { Component, PropTypes } from 'react';
import { asyncConnect } from 'redux-async-connect';
import { connect } from 'react-redux';

import { load as loadStandings, isLoaded as isStandingsLoaded } from 'redux/modules/standings';
import * as standingsActions from 'redux/modules/standings';

import { load as loadNews, isLoaded as isNewsLoaded } from 'redux/modules/news';
import * as newsActions from 'redux/modules/news';

import { load as loadArticles, isLoaded as isArticlesLoaded } from 'redux/modules/articlesTiles';
import * as articlesActions from 'redux/modules/articlesTiles';

import { load as loadPlayerStats, isLoaded as isPlayerStatsLoaded } from 'redux/modules/playerStats';
import * as playerStatsActions from 'redux/modules/playerStats';

import { ArticleTilesList } from 'containers';
import { scheduleByDaySelector, standingsGroupsSelector, standingsPlayOffSelector } from 'helpers/selectors';
import config from 'config';

import {
  ScheduleWidget,
  StandingsWidget,
  Meta,
  LatestNews,
  Ad,
  TopPlayersBlock,
  Copyrights
} from 'components';

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];

    const state = getState();

    if (!isStandingsLoaded(state)) {
      promises.push(dispatch(loadStandings()));
    }

    if (!isArticlesLoaded(state)) {
      promises.push(dispatch(loadArticles()));
    }

    if (!isNewsLoaded(state)) {
      promises.push(dispatch(loadNews()));
    }

    ['goalpass', 'assistent', 'keeper'].forEach(section => {
      if (!isPlayerStatsLoaded(state, section)) {
        promises.push(dispatch(loadPlayerStats(section)));
      }
    });

    return Promise.all(promises);
  }
}])
@connect(
  state => ({
    // standings
    groups: standingsGroupsSelector(state),
    playoff: standingsPlayOffSelector(state),
    news: state.news.items,
    standings: state.standings.data,
    standingsLoading: state.standings.loading,
    standingsError: state.standings.error,
    // schedule
    scheduleByDay: scheduleByDaySelector(state),
    // todo: this is not used, ArticleTilesList has its own @connect
    // articles
    articles: state.articles,
    articlesLoading: state.articles.loading,
    articlesError: state.articles.error,
    // players stats
    topPlayers: state.playerStats,

    dispatch: PropTypes.func
  }), {
    ...standingsActions,
    ...articlesActions,
    ...newsActions,
    ...playerStatsActions
  }
)
export default class Home extends Component {
  static propTypes = {
    standings: PropTypes.object,
    isStandingsLoaded: PropTypes.bool,
    // todo: same here
    articles: PropTypes.object,
    news: PropTypes.array,
    // todo: and here
    articlesLoading: PropTypes.bool,
    scheduleByDay: PropTypes.object,
    groups: PropTypes.array,
    playoff: PropTypes.array,
    topPlayers: PropTypes.object,
    location: PropTypes.object
  };

  render() {
    const {scheduleByDay, groups, playoff, topPlayers} = this.props;
    const copyrightsData = [
      {
        title: 'Последние новости чемпионата мира по хоккею 2016 (ЧМХ-2016) на Рамблере',
        text: 'Хоккейный турнир начнется 6 мая и завершится 22 мая в Москве на «ВТБ Арене». На Рамблере вы найдете расписание матчей, турнирную таблицу, а также статьи и последние новости со спортивных площадок. Статистика группового этапа и игр плей-офф также доступна и удобно отображается на мобильных устройствах и планшетах. Специально для всех поклонников спорта мы подготовили онлайн-трансляции матчей чемпионата мира по хоккею, а для тех, у кого ограничен доступ к высокоскоростному интернету, мы ведем текстовую трансляцию, которую вы найдете на страницах «Протокол матча».'
      }
    ];
    return (
      <div className="section-left justify-flex">
        <Meta data={{
          title: config.app.title,
          ogTitle: 'Чемпионат мира по хоккею 2016 в России',
          description: config.app.description,
          ogDescription: 'Чемпионат мира по хоккею 2016 в России: расписание матчей, онлайн трансляции, результаты, новости',
          keywords: 'Чемпионат мира по хоккею 2016 в России расписание результаты турнирная таблица чм чмх Санкт-Петербург Москва',
          pathname: this.props.location.pathname,
          robots: 'all'
        }} />
        <div style={{overflow: 'hidden', width: '100%'}}> {/* need overflow wrapper */}
          <ScheduleWidget schedule={scheduleByDay}/>
          <StandingsWidget groups={groups} playoff={playoff}/>
        </div>
        <ArticleTilesList id="ArticleBlockFeatured" />
        <ArticleTilesList id="ArticlesBlockFirst" />
        <LatestNews news={this.props.news.slice(0, 5)} />
        <Ad id="superfooter" mobileId="listing" mobileIndex={1}/>
        <TopPlayersBlock data={topPlayers} />
        <ArticleTilesList id="ArticlesBlockSecond" />
        <Copyrights copyrightsData={copyrightsData} id="Copyrights" />
      </div>
    );
  }
}
