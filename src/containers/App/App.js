import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { asyncConnect } from 'redux-async-connect';
import * as moment from 'moment';
import * as Nprogress from 'nprogress';
import { load as loadSchedule, isLoaded as isScheduleLoaded } from 'redux/modules/schedule';
import * as scheduleActions from 'redux/modules/schedule';
import {
  Ad,
  Navbar,
  Header,
  Footer
} from 'components';

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];

    // we need schedule data on all pages
    if (!isScheduleLoaded(getState())) {
      promises.push(dispatch(loadSchedule()));
    }

    return Promise.all(promises);
  }
}])
@connect(
  state => ({
    schedule: state.schedule.data,
    scheduleLoading: state.schedule.loading,
    scheduleError: state.schedule.error
  }), {...scheduleActions}
)

export default class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    location: PropTypes.object
  };

  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  componentWillMount() {
    if (typeof window === 'undefined') {
      return;
    }
    // window.Adf.util.debugBase = true;

    window.addEventListener('resize', this.fixViewport.bind(this));
    this.fixViewport();
  }
  componentWillReceiveProps(nextProps) {
    window.clearAllIntervals();
    window.clearAllTimeouts();
    Nprogress.done();
    // redraw ads on location change
    // only if location really changed
    // setTimeout removes this instruction from the stack and allows components to refresh window.ads
    if (this.props.location.pathname !== nextProps.location.pathname) {
      setTimeout(() => {
        global.ads.map(ad => ad.redraw());
      }, 0);
    }
  }
  fixViewport() {
    const metaViewport = document.querySelector('meta[name="viewport"]');
    const width = window.outerWidth || screen.width || window.innerWidth;

    if (width > 620) {
      metaViewport.setAttribute('content', 'width=device-width, initial-scale=1');
    } else {
      const scale = Math.round(width / 620 * 1000) / 1000;
      metaViewport.setAttribute('content', 'width=620, user-scalable=no, initial-scale=' + scale);
    }
  }

  render() {
    moment.locale('ru');

    return (
      <div>
        <Ad id="billboard" className="ad ad--no-gap" />
        <Navbar location={this.props.location} />
        <Header />
        <main className="app" id="app">
          <section className="justify-flex">
            <Ad mobileId="topbanner" />
            <section className="section-left justify-flex">
              {this.props.children}
            </section>
            <section className="section-right">
              <Ad id="240x400" />
              <Ad id="240x200" />
            </section>
          </section>
        </main>
        <Footer />
      </div>
    );
  }
}
