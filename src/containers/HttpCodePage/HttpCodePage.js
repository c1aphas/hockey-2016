import React, { Component, PropTypes } from 'react';
import NestedStatus from 'react-nested-status';

export default class HttpCodePage extends Component {
  static propTypes = {
    code: PropTypes.number,
    message: PropTypes.string
  };

  render() {
    return (
      <NestedStatus code={this.props.code}>
        <div className="block2 block2--with-gap http-code">
          <h1>{this.props.code}</h1>
          <h3>{this.props.message}</h3>
        </div>
      </NestedStatus>
    );
  }
}
