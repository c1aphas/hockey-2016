import React, { Component, PropTypes } from 'react';
import {getCountryCode } from 'helpers/Countries';
import { Flag, MatchLink } from 'components';
import * as _ from 'lodash';
import classNames from 'classnames';

class Score extends Component {
  static propTypes = {
    match: PropTypes.object,
    blank: PropTypes.bool,
    result: PropTypes.object,
    reverse: PropTypes.bool,
    base_team: PropTypes.number,
  }
  getResult() {
    if (!this.props.result) {
      return '-:-';
    }
    const part1 = this.props.result.detailed.goal1;
    const part2 = this.props.result.detailed.goal2;
    if (this.props.reverse) {
      return part2 + ':' + part1;
    }
    return part1 + ':' + part2;
  }
  render() {
    const classnames = classNames('group-row__score group-versus group-row__value', {
      'group-row__value--blank': this.props.blank
    });
    return (<td className={classnames}>
              <MatchLink match={this.props.match}><div className="group-row__score-value">{this.getResult()}</div></MatchLink>
            </td>);
  }
}

class Row extends Component {
  static propTypes = {
    data: PropTypes.object,
    commands: PropTypes.array,
    versus: PropTypes.object
  };

  getScores() {
    const self = this;
    return _.map(this.props.commands, (command) => {
      if (!this.props.versus) {
        return <Score blank {...command} key={command.n} />;
      }
      const match = self.props.versus[command.team_id];

      let result;
      let teams;
      let reverse;
      if (!!match) {
        result = match.result;
        teams = match.teams;

        reverse = teams[0].id !== self.props.data.team_id;
      }
      return (<Score match={match} blank={command.team_id === self.props.data.team_id} result={result} reverse={reverse} teams={teams} {...command} key={command.n} />);
    });
  }

  render() {
    const data = this.props.data;
    return (<tr className="group-row">
              <td className="group-row__team">
                <div className="group-row__team-place">{data.n}</div>
                  <Flag name={getCountryCode(data.name) || ''} classes="group-row__team-flag" />
                <div className="group-row__team-title">{getCountryCode(data.name)}</div>
              </td>
              {this.getScores()}

              <td className="group-row__value">{data.game}</td>
              <td className="group-row__value">{data.win}</td>
              <td className="group-row__value">{data.wino}</td>
              <td className="group-row__value">{data.winb}</td>
              <td className="group-row__value">{data.loseo}</td>
              <td className="group-row__value">{data.loseb}</td>
              <td className="group-row__value">{data.lose}</td>
              <td className="group-row__value">{data.goal1 - data.goal2}</td>
              <td className="group-row__value group-row__value--score">{data.points}</td>
            </tr>);
  }
}


export default class StandingsGroup extends Component {
  static propTypes = {
    group: PropTypes.object,
    matches: PropTypes.array
  };
  getTeams() {
    return _.map(this.getItems(), (item) => {
      return (<td className="group-versus" key={item.team_id}>
                <Flag name={getCountryCode(item.name) || ''} size="small" />
              </td>);
    });
  }
  getVersusFor(teamId) {
    const unsorted = _.filter(this.props.matches, (item) => {
      return item.teams[0].id === teamId || item.teams[1].id === teamId;
    });

    return _.keyBy(unsorted, (item) => {
      if (item.teams[0].id === teamId) {
        return item.teams[1].id;
      }
      return item.teams[0].id;
    });
  }
  getItems() {
    if (!this.props.group.data) {
      return [];
    }

    return this.props.group.data.data;
  }
  getRows() {
    const self = this;
    const items = this.getItems();

    return _.map(items, (item) => {
      return <Row data={item} key={item.team_id} versus={self.getVersusFor(item.team_id)} commands={items} />;
    });
  }
  render() {
    return (<div className="block2__content group-item">
             <table className="group-item__table">
               <tbody>
                 <tr className="group-top-labels">
                   <td className="group-top-labels__first">КОМАНДЫ</td>
                       {_.map(this.getItems(), (item) => {
                         return <td className="group-versus group-top-labels__versus" key={item.n}>{item.n}</td>;
                       })}
                   <td>И</td>
                   <td colSpan="3" className="group-top-labels__tripple">Победы</td>
                   <td colSpan="3" className="group-top-labels__tripple">Поражения</td>
                   <td>РШ</td>
                   <td>О</td>
                 </tr>
                 <tr className="group-top-teams">
                   <td></td>
                   {this.getTeams()}

                   <td className="group-top-teams__label group-top-teams__label--blank"></td>
                   <td className="group-top-teams__label group-top-teams__label--border-left"></td>
                   <td className="group-top-teams__label">ОТ</td>
                   <td className="group-top-teams__label group-top-teams__label--border-right">ПБ</td>
                   <td className="group-top-teams__label">ПБ</td>
                   <td className="group-top-teams__label">ОТ</td>
                   <td className="group-top-teams__label"></td>
                   <td className="group-top-teams__label group-top-teams__label--blank"></td>
                   <td className="group-top-teams__label group-top-teams__label--blank"></td>
                  </tr>
                {this.getRows()}
               </tbody>
             </table>
            </div>);
  }
}
