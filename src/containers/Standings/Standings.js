import React, { Component, PropTypes } from 'react';
import { asyncConnect } from 'redux-async-connect';
import { connect } from 'react-redux';
import { load as loadStandings, isLoaded as isStandingsLoaded } from 'redux/modules/standings';

import * as standingsActions from 'redux/modules/standings';
import { load as loadSchedule, isLoaded as isScheduleLoaded } from 'redux/modules/schedule';
import * as scheduleActions from 'redux/modules/schedule';
import * as _ from 'lodash';
import { Meta, Ad, Copyrights } from 'components';

import StandingsGroup from 'containers/Standings/StandingsGroup';
import Playoff from 'containers/Standings/Playoff';
import GroupHeader from 'containers/Standings/GroupHeader';

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];

    if (!isStandingsLoaded(getState())) {
      promises.push(dispatch(loadStandings()));
    }

    if (!isScheduleLoaded(getState())) {
      promises.push(dispatch(loadSchedule()));
    }

    return Promise.all(promises);
  }
}])
@connect(
  state => ({
    standings: state.standings.data,
    standingsLoading: state.standings.loading,
    standingsError: state.standings.error,

    schedule: state.schedule.data,
    scheduleLoading: state.schedule.loading,
    scheduleError: state.schedule.error

  }), {...standingsActions, ...scheduleActions}
)
export default class Standings extends Component {
  static propTypes = {
    standings: PropTypes.object,
    schedule: PropTypes.object,
    location: PropTypes.object
  };

  getCalendarGroups(groups) {
    if (!this.props.schedule) {
      return {};
    }

    return _.pick(_.groupBy(this.props.schedule.data, (item) => {
      return item.group.id;
    }), groups);
  }
  getPlayoffGroups() {
    if (!this.props.standings) {
      return [];
    }

    return _.filter(this.props.standings.data, (standing) => {
      return standing.type === 'tournament.table.playoff';
    });
  }
  getStandingsGroups() {
    if (!this.props.standings) {
      return [];
    }

    return _.filter(this.props.standings.data, (standing) => {
      return standing.type === 'tournament.table.group';
    });
  }

  render() {
    const standingsGroups = this.getStandingsGroups();
    const calendarGroups = this.getCalendarGroups(_.map(standingsGroups, (item) => {
      return item.group_id;
    }));
    const playoffGroups = this.getPlayoffGroups();

    const groups = [];
    const copyrightsData = [
      {
        title: 'Статистика группового тура и плей-офф чемпионата мира по хоккею 2016',
        text: 'Подробная статистика игр группы А и группы Б чемпионата мира по хоккею включает данные о количестве игр, победах, поражениях, общему количеству очков, а также шайбах, забитых в овертайме.'
      },
      {
        title: 'Статистика плей-офф ЧМХ-2016',
        text: '19 мая начинаются первые игры плей-офф, а вечером 22 мая определятся финалисты турнира ЧМХ-2016. У нас вы сможете узнать статистику текущих матчей игр чемпионата мира, а также прочитать прогнозы специалистов на финальные игры.'
      }
    ];

    _.forEach(standingsGroups, (item, index) => {
      groups.push(<GroupHeader name={item.data.name} bordered={index !== 0} key={'group_header' + index} />);
      groups.push(<StandingsGroup key={'standing_group' + index} group={item} matches={calendarGroups[item.group_id]} />);
    });

    let playoff;
    if (playoffGroups) {
      playoff = <Playoff data={playoffGroups} />;
    }

    return (
      <div>
        <Meta data={{
          title: 'Расписание игр на чемпионате мира по хоккею 2016 - Рамблер',
          ogTitle: 'Турнирная таблица Чемпионата мира по хоккею 2016',
          description: 'Турнирная таблица чемпионата мира по хоккею 2016 года.',
          ogDescription: 'Чемпионат мира по хоккею 2016 в России: турнирная таблица и результаты игр',
          keywords: 'Чемпионат мира по хоккею 2016 в России турнирная таблица результаты Чехия Швеция Россия Финляндия Канада Франция Беларусь США рейтинговая сетка место',
          pathname: this.props.location.pathname,
          robots: 'all'
        }} />

        <div className="block2">
          {groups}
        </div>
        <Ad id="superfooter" mobileId="listing" mobileIndex={1}/>
        {playoff}
        <Ad id="context" mobileId="listing" mobileIndex={2}/>
        <Copyrights copyrightsData={copyrightsData} id="Copyrights" />
      </div>
    );
  }
}
