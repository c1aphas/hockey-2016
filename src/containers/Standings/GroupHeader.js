import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';

export default class GroupHeader extends Component {
  static propTypes = {
    bordered: PropTypes.bool,
    name: PropTypes.string
  }
  render() {
    const classes = classNames('block2__header', {
      'block2__header--bordered': this.props.bordered
    });
    return (<div className={classes}>{this.props.name}</div>);
  }
}
