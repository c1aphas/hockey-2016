import React, { Component, PropTypes } from 'react';
import {getCountryCode } from 'helpers/Countries';
import { Flag, PlayOffMatchLink } from 'components';
import * as _ from 'lodash';

import classNames from 'classnames';
import GroupHeader from 'containers/Standings/GroupHeader';


class PlayoffMatch extends Component {
  static propTypes = {
    data: PropTypes.object
  };
  getScore() {
    const { played, tgoal1, tgoal2 } = this.props.data;
    return played
      ? tgoal1 + ':' + tgoal2
      : '';
  }
  getTeam1Name() {
    if (this.props.data && this.props.data.team[0].team1) {
      return this.props.data.team[0].team1;
    }
    return '';
  }
  getTeam2Name() {
    if (this.props.data && this.props.data.team[0].team2) {
      return this.props.data.team[0].team2;
    }
    return '';
  }
  isFuture() {
    return !!this.props.data && this.props.data.played === 0;
  }
  isPlayed() {
    return !!this.props.data && this.props.data.played === 1;
  }
  render() {
    const team1Name = this.getTeam1Name();
    const team2Name = this.getTeam2Name();
    const classnames = classNames('play-off-match', {'play-off-match--future': this.isFuture(),
                                                     'play-off-match--past': this.isPlayed()});
    return (<PlayOffMatchLink tag="div" match={this.props.data} className={classnames}>
              <div className="play-off-match__side play-off-match-side play-off-match-side--left">
                <Flag name={getCountryCode(team1Name) || ''} size="small" classes="play-off-match-side__flag" />
                <div className="play-off-match-side__country">{getCountryCode(team1Name) || team1Name}</div>
              </div>
              <div className="play-off-match__score">
                {this.getScore()}
              </div>
              <div className="play-off-match__side play-off-match-side--right">
                <Flag name={getCountryCode(team2Name) || ''} size="small" classes="play-off-match-side__flag" />
                <div className="play-off-match-side__country">{ getCountryCode(team2Name) || team2Name }</div>
              </div>
            </PlayOffMatchLink>);
  }
}

export default class Playoff extends Component {
  static propTypes = {
    data: PropTypes.array
  }

  render() {
    return (<div className="block2 block2--with-gap">
              <GroupHeader name="Плейофф" />
               <div className="play-off">
                 <div className="play-off__row play-off__row--less-bottom">
                   <div className="play-off__group">
                      <div className="play-off__header">1/4 ФИНАЛА</div>
                      <PlayoffMatch data={_.get(this.props.data, '[0].data.data[0]') } />
                      <PlayoffMatch data={_.get(this.props.data, '[0].data.data[1]') } />
                   </div>
                   <div className="play-off__group">
                      <div className="play-off__header">1/4 ФИНАЛА</div>
                      <PlayoffMatch data={_.get(this.props.data, '[0].data.data[2]') } />
                      <PlayoffMatch data={_.get(this.props.data, '[0].data.data[3]') }/>
                   </div>
                 </div>

                 <div className="play-off-delim play-off-delim--double"></div>

                 <div className="play-off__row">
                   <div className="play-off__group">
                     <div className="play-off__header">1/2 ФИНАЛА</div>
                     <PlayoffMatch data={_.get(this.props.data, '[1].data.data[0]')} />
                   </div>
                   <div className="play-off__group">
                     <div className="play-off__header">1/2 ФИНАЛА</div>
                     <PlayoffMatch data={_.get(this.props.data, '[1].data.data[1]') }/>
                   </div>
                 </div>
                 <div className="play-off-delim play-off-delim--double"></div>
                 <div className="play-off__row">
                   <div className="play-off__group">
                     <div className="play-off__header">МАТЧ ЗА 3 МЕСТО</div>
                     <PlayoffMatch data={_.get(this.props.data, '[2].data.data[0]')}/>
                   </div>
                 </div>
                 <div className="play-off-delim"></div>
                   <div className="play-off__row">
                    <div className="play-off__group">
                     <div className="play-off__header">ФИНАЛ</div>
                     <PlayoffMatch data={_.get(this.props.data, '[3].data.data[0]')} />
                    </div>
                   </div>
                 </div></div>);
  }
}
