import React, { Component, PropTypes } from 'react';
import { asyncConnect } from 'redux-async-connect';
import { connect } from 'react-redux';
import { load, isLoaded } from 'redux/modules/publication';
import { NotFound } from 'containers';
import { Publication, Meta, Ad } from 'components';
import { URL_IMG_CHAMPIONAT } from 'helpers/Constants';

function parseUrlSegments(input) {
  // can we get it from params directly? NO.
  // Because react-router doesn't support /(:multiple)-(:params)/per/segment
  const segments = input.match(/^([a-z0-9]+)-(.*)$/i);
  if (!segments || segments.length < 3) {
    return {};
  }

  return {
    id: segments[1],
    slug: segments[2]
  };
}

function getAsyncConnector() {
  return app => {
    const segments = parseUrlSegments(app.params.request);
    if (!segments.id) {
      return Promise.all();
    }

    const state = app.store.getState();

    if (isLoaded(state, segments.id)) {
      return Promise.all();
    }

    const fetcher = load(segments.id, segments.slug);
    const pr = app.store.dispatch(fetcher);
    return Promise.all([pr]);
  };
}

@asyncConnect([{
  promise: getAsyncConnector()
}])
@connect(
  state => ({
    publications: state.publication.items
  })
)
export default class extends Component {
  static propTypes = {
    params: PropTypes.object,
    publications: PropTypes.object,
    route: PropTypes.object,
    location: PropTypes.object
  };

  getPubType() {
    const reqType = this.props.route.path.split('/').shift().toLowerCase();
    return reqType === 'news' ? 'news' : 'article'; // slightly derp
  }

  render() {
    const params = parseUrlSegments(this.props.params.request);
    if (!params.id) {
      return <NotFound />;
    }

    const pubState = this.props.publications[params.id];
    if (!pubState || !pubState.loaded || !pubState.pub || pubState.error) {
      return <NotFound />;
    }

    const pub = pubState.pub;

    if (this.getPubType() !== pub.type) {
      return <NotFound />;
    }

    if (params.slug !== pub.translit) {
      return <NotFound />;
    }

    let description = '';
    let keywords = '';
    let image;
    if (pub.type === 'news') {
      description = (pub.content[0].text || '').replace(/(<([^>]+)>)/g, ''); // first paragraph in plain text
      keywords = 'Чемпионат мира по хоккею 2016, ЧМХ, последние новости';
    } else {
      description = pub.subtitle;
      image = pub.image ? `${URL_IMG_CHAMPIONAT}/${pub.image.url}` : null;
      keywords =
        // first 20 words of the first paragraph, lowercased, no punctuation
        (pub.content[0].text || '')
        .toLowerCase()
        .replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g, '')
        .split(' ')
        .filter(it => it.length > 3)
        .slice(0, 20)
        .join(' ');
    }

    return (
      <div>
        <Meta data={{
          title: `${pub.title} ЧМХ 2016 - Рамблер`,
          ogTitle: pub.title,
          description,
          ogDescription: description,
          ogType: 'article',
          image,
          keywords,
          pathname: this.props.location.pathname,
          robots: 'noindex, follow'
        }} />

        <Publication pub={pub} />
        <Ad id="superfooter" mobileId="content" type="content"/>
      </div>
    );
  }
}
