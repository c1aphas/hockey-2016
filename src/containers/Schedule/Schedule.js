import React, { Component, PropTypes } from 'react';
import { asyncConnect } from 'redux-async-connect';
import { connect } from 'react-redux';
import { load as loadSchedule, isLoaded as isScheduleLoaded } from 'redux/modules/schedule';
import * as scheduleActions from 'redux/modules/schedule';
import * as _ from 'lodash';
import ScheduleItem from 'containers/Schedule/ScheduleItem';
import { Meta, Ad, Copyrights} from 'components';

class Delim extends Component {

  render() {
    return (<tr className="schedule-full-delim">
              <td></td>
              <td className="schedule-full-delim__td"></td>
            </tr>
           );
  }
}


@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];

    if (!isScheduleLoaded(getState())) {
      promises.push(dispatch(loadSchedule()));
    }

    return Promise.all(promises);
  }
}])
@connect(
  state => ({
    schedule: state.schedule.data,
    scheduleLoading: state.schedule.loading,
    scheduleError: state.schedule.error
  }), {...scheduleActions}
)
export default class extends Component {
  static propTypes = {
    schedule: PropTypes.object,
    location: PropTypes.object
  };

  getGroups() {
    if (!this.props.schedule) {
      return {};
    }
    return _.groupBy(this.props.schedule.data, (item) => {
      return item.date;
    });
  }
  render() {
    const groups = [];
    const copyrightsData = [
      {
        title: 'Расписание игр чемпионата мира по хоккею 2016',
        text: 'Хоккейный турнир ЧМ-2016 будет проходить в течение двух недель с 6 по 22 мая на двух площадках Москвы и Санкт-Петербурга. 16 команд, разбитых на две группы, будут соревноваться за выход в плей-офф, который начнется 19 мая. 22 мая пройдут игры за первые три места, на которых определится победитель турнира.'
      },
      {
        title: 'Прямая трансляция матчей чемпионата мира по хоккею',
        text: 'Каждый день будет проходить от четырех до шести игр, онлайн-трансляции которых можно смотреть на Рамблере. Помимо прямого эфира трансляций матчей мы также ведем тестовые трансляции с подробной статистикой и комментариями специалистов.'
      }
    ];

    if (this.props.schedule) {
      _.forEach(this.getGroups(), (items, date) => {
        _.forEach(items, (item, index) => {
          groups.push(<ScheduleItem is_first={index === 0} key={item._id} match={item} items_length={items.length} rawStatus={item.raw_status} {...item} />);
          if (index === items.length - 1) {
            groups.push(<Delim key={'delim' + date} />);
          }
        });
      });
    }
    return (
      <div>
        <div className="block2 block2--with-gap schedule-full">
          <Meta data={{
            title: 'Расписание игр на чемпионате мира по хоккею 2016 - Рамблер',
            ogTitle: 'Расписание игр на чемпионате мира по хоккею 2016',
            description: 'Расписание игр на чемпионате мира по хоккею 2016 года. 6 мая США - Канада, Швеция - Латвия, Чехия - Россия, Финляндия - Беларусь',
            ogDescription: 'Смотрите расписание игр на Чемпионате мира по хоккею 2016 в России',
            keywords: 'Чемпионат мира по хоккею 2016 в России расписание игр результаты турнирная таблица Санкт-Петербург Москва Чехия Швеция Россия Финляндия Канада Франция Беларусь США',
            pathname: this.props.location.pathname,
            robots: 'all'
          }} />

          <div className="block2__header">Расписание</div>
          <div className="block2__content schedule-full__content">
            <table className="schedule-full__table">
              <tbody>
                {groups}
              </tbody>
            </table>
          </div>
        </div>
        <Ad id="superfooter" mobileId="listing" type="footer"/>
        <Copyrights copyrightsData={copyrightsData} id="Copyrights" />
      </div>);
  }
}
