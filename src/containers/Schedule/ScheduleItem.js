import React, { Component, PropTypes } from 'react';
import classNames from 'classnames';
import { Flag, MatchLink } from 'components';
import moment from 'moment';
import { includes } from 'lodash';
import {getCountryCode } from 'helpers/Countries';

import {
  MATCH_STATE_1T, MATCH_STATE_2T, MATCH_STATE_3T
} from 'helpers/MatchStates';

// 'ntl' => '',
// 'dns' => 'не начался',
// 'post' => 'перенесён',
// 'delay' => 'остановлен',
// '1t' => '1-й период',
// 'half' => 'перерыв',
// '2t' => '2-й период',
// '3t' => '3-й период',
// 'extra' => 'овертайм',
// 'pen' => 'буллиты',
// 'fin' => 'окончен'

export default class ScheduleItem extends Component {
  static propTypes = {
    match: PropTypes.object, //TODO: leave only object
    is_first: PropTypes.bool,
    rawStatus: PropTypes.string,
    date: PropTypes.string,
    result: PropTypes.object,
    tour: PropTypes.number,
    group: PropTypes.object,
    time: PropTypes.string,
    status: PropTypes.string,
    teams: PropTypes.array,
    items_length: PropTypes.number
  };

  getDate() {
    if (this.isFirst()) {
      const date = moment(this.props.date);
      return (<td rowSpan={this.props.items_length} className="schedule-full-row__date">
               <div className="schedule-full-row__date-day">{date.format('DD')}</div>
              <div className="schedule-full-row__date-month">{date.format('DD MMMM').slice(3)}</div>
              </td>);
    }
    return null;
  }

  getResult() {
    if (this.isScheduled()) {
      return '-:-';
    }
    const detailed = this.props.result.detailed;
    return detailed.goal1 + ':' + detailed.goal2;
  }
  getStage() {
    if (this.props.tour) {
      return this.props.tour + ', ' + this.props.group.name;
    }

    return this.props.group.name;
  }
  getShortCountryName(team) {
    return getCountryCode(team.name) || '';
  }
  getStatus() {
    if (this.isPast()) {
      return 'ЗАВЕРШЕН'; // COSTbIL
    }
    const lives = {
      [MATCH_STATE_1T]: 'I ПЕРИОД',
      [MATCH_STATE_2T]: 'II ПЕРИОД',
      [MATCH_STATE_3T]: 'III ПЕРИОД'
    };

    return typeof lives[this.props.rawStatus] !== 'undefined' ? lives[this.props.rawStatus] : this.props.status;
  }

  getStatusTime() {
    if (this.isLive()) {
      return this.getStatus();
    }
    if (this.isScheduled()) {
      return this.props.time;
    }
    if (this.isPast()) {
      return this.getStatus();
    }
    return this.props.status;
  }

  isFirst() {
    return this.props.is_first;
  }
  isLive() {
    return this.testSchedule(this.props.rawStatus, ScheduleItem.scheduleTypes.alive);
  }
  isScheduled() {
    return this.testSchedule(this.props.rawStatus, ScheduleItem.scheduleTypes.schedule);
  }
  isPast() {
    return this.testSchedule(this.props.rawStatus, ScheduleItem.scheduleTypes.past);
  }

  testSchedule(rawStatus, items) {
    return includes(items, rawStatus);
  }

  static scheduleTypes = {
    'alive': ['delay', '1t', 'half', '2t', '3t', 'extra', 'pen'],
    'schedule': ['ntl', 'dns', 'post'],
    'past': ['fin']
  };

  render() {
    const classnames = classNames('schedule-full-row', {'schedule-full-row--live': this.isLive(),
                                                        'schedule-full-row--past': this.isPast(),
                                                        'schedule-full-row--scheduled': this.isScheduled()
                                                       });
    const team1Code = this.getShortCountryName(this.props.teams[0]);
    const team2Code = this.getShortCountryName(this.props.teams[1]);

    return (<MatchLink tag="tr" match={this.props.match} className={classnames}>
              {this.getDate()}
              <td className="schedule-full-row__time">{this.props.time}</td>
              <td className="schedule-full-row__status-time">{this.getStatusTime()}</td>
              <td className="schedule-full-row__stage">{this.getStage()}</td>
              <td className="schedule-full-row__status">{this.getStatus()}</td>
              <td className="schedule-full-row__side1">{team1Code}</td>
              <td className="schedule-full-row__flag"><Flag name={team1Code} size="small" /></td>
              <td className="schedule-full-row__score"><div className="schedule-full-row__score-text">{this.getResult()}</div></td>
              <td className="schedule-full-row__flag"><Flag name={team2Code} size="small" /></td>
              <td className="schedule-full-row__side2">{team2Code}</td></MatchLink>);
  }
}
