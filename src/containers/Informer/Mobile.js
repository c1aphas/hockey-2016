import React, { Component, PropTypes } from 'react';
import { asyncConnect } from 'redux-async-connect';
import { connect } from 'react-redux';
import { load as loadStandings, isLoaded as isStandingsLoaded } from 'redux/modules/standings';
import * as standingsActions from 'redux/modules/standings';
import { load as loadSchedule, isLoaded as isScheduleLoaded } from 'redux/modules/schedule';
import * as scheduleActions from 'redux/modules/schedule';
import { AnnouncementWidget } from 'containers';
import { TeamProgressMobile, Flag, KLink } from 'components';

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];

    const state = getState();

    if (!isStandingsLoaded(state)) {
      promises.push(dispatch(loadStandings()));
    }

    if (!isScheduleLoaded(getState())) {
      promises.push(dispatch(loadSchedule()));
    }

    return Promise.all(promises);
  }
}])
@connect(
  state => ({
    standings: state.standings.data.data
  }), {
    ...scheduleActions,
    ...standingsActions
  }
)
export default class Mobile extends Component {
  static propTypes = {
    standings: PropTypes.array
  };

  render() {
    return (
      <div className="rambler-hockey-mobile-informer">
        <div className="rambler-hockey-mobile-informer__header">
          <KLink to="/" className="rambler-hockey-mobile-informer__title">
            <h3>Чемпионат мира по хоккею 2016</h3>
          </KLink>
        </div>
        <div className="rambler-hockey-mobile-informer__country">
          <Flag name="RUS" size="small" />
          Россия
        </div>
        <TeamProgressMobile data={this.props.standings} team="Россия" />
        <div className="rambler-hockey-mobile-informer__announcement">
          <AnnouncementWidget />
        </div>
      </div>
    );
  }
}
