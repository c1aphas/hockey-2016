import React, { Component, PropTypes } from 'react';
import { asyncConnect } from 'redux-async-connect';
import { connect } from 'react-redux';

import { load as loadStandings, isLoaded as isStandingsLoaded } from 'redux/modules/standings';
import * as standingsActions from 'redux/modules/standings';

import { load as loadSchedule, isLoaded as isScheduleLoaded } from 'redux/modules/schedule';
import * as scheduleActions from 'redux/modules/schedule';

import { load as loadNews, isLoaded as isNewsLoaded } from 'redux/modules/news';
import * as newsActions from 'redux/modules/news';

import { load as loadArticles, isLoaded as isArticlesLoaded } from 'redux/modules/articlesTiles';
import * as articlesActions from 'redux/modules/articlesTiles';

import moment from 'moment';
// import { URL_RAMBLER_SPORT } from 'helpers/Constants';
// import Helmet from 'react-helmet';

import { AnnouncementWidget } from 'containers';

import {
  LatestNews,
  ArticleTiles,
  TeamProgress,
  KLink
} from 'components';

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];

    const state = getState();

    if (!isStandingsLoaded(state)) {
      promises.push(dispatch(loadStandings()));
    }

    if (!isScheduleLoaded(getState())) {
      promises.push(dispatch(loadSchedule()));
    }

    if (!isArticlesLoaded(state)) {
      promises.push(dispatch(loadArticles()));
    }

    if (!isNewsLoaded(state)) {
      promises.push(dispatch(loadNews()));
    }

    return Promise.all(promises);
  }
}])
@connect(
  state => ({
    news: state.news.items,
    articles: state.articlesTiles,
    standings: state.standings.data.data
  }), {
    ...articlesActions,
    ...newsActions,
    ...scheduleActions,
    ...standingsActions
  }
)
export default class Desktop extends Component {
  static propTypes = {
    articles: PropTypes.object,
    news: PropTypes.array,
    standings: PropTypes.array
  };

  render() {
    const news = (this.props.news || []).slice(0, 3);
    const topArticle = this.props.articles.loaded ? this.props.articles.data.articleFeature : {};

    // <Helmet base={{ target: '_parent', href: URL_RAMBLER_SPORT }} />

    return (
      <div className="rambler-hockey-informer">
        <div className="rambler-hockey-informer__inner">
          <div className="rambler-hockey-informer__header">
            <KLink to="/" className="rambler-hockey-informer__title">
              <h1>Чемпионат мира по хоккею 2016</h1>
            </KLink>
            <h3 className="rambler-hockey-informer__date">{moment().format('D MMMM')}</h3>
          </div>
          <TeamProgress data={this.props.standings} team="Россия" />
          <div className="rambler-hockey-informer__blocks">
            <div className="rambler-hockey-informer__announcement">
              <KLink className="rambler-hockey-informer__announcement-player" to="/" />
              <AnnouncementWidget />
            </div>
            <div className="rambler-hockey-informer__article">
              <ArticleTiles article={topArticle} />
            </div>
            <div className="rambler-hockey-informer__news">
              <LatestNews news={news} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
