import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { load as loadMoreArticles, rollBack } from 'redux/modules/articlesTiles';
import * as articlesActions from 'redux/modules/articlesTiles';
import { ArticlesBlockFirst, ArticlesBlockSecond, ArticleBlockFeatured } from 'components';
import Scroll from 'helpers/Scroll';

@connect(
  state => ({
    articleFeature: state.articlesTiles.data.articleFeature,
    articlesBlockFirst: state.articlesTiles.data.articlesBlockFirst,
    articlesBlockSecond: state.articlesTiles.data.articlesBlockSecond,
    articlesLoading: state.articlesTiles.loading
  }), {...articlesActions, ...{ loadMoreArticles: loadMoreArticles }, ...{ rollBack: rollBack } }
)

export default class ArticleTilesList extends Component {
  static propTypes = {
    id: PropTypes.string,
    articles: PropTypes.object,
    dispatch: PropTypes.func,
    articlesLoading: PropTypes.bool,
    loadMoreArticles: PropTypes.func,
    rollBack: PropTypes.func
  };

  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  componentDidUpdate() {
    Scroll.scrollRelease();
  }

  componentWillUnmount() {
    this.context.store.dispatch(rollBack);
  }

  getLoadMoreHandler(lastId) {
    return () => {
      Scroll.scrollLock();
      this.props.loadMoreArticles(lastId);
    };
  }

  render() {
    const articles = this.props;
    const id = articles.id;
    const articleFeature = articles.articleFeature;
    const articlesBlockFirst = articles.articlesBlockFirst;
    const articlesBlockSecond = articles.articlesBlockSecond;
    const lastItem = articlesBlockSecond[articlesBlockSecond.length - 1] || {};
    const isLoading = this.props.articlesLoading;

    const blocks = {
      'ArticleBlockFeatured': <ArticleBlockFeatured articles={articleFeature} />,
      'ArticlesBlockFirst': <ArticlesBlockFirst articles={articlesBlockFirst} />,
      'ArticlesBlockSecond': <ArticlesBlockSecond articles={articlesBlockSecond} loadMore={this.getLoadMoreHandler(lastItem._id)} isLoading={isLoading} />
    };

    return (
      blocks[id]
    );
  }
}
