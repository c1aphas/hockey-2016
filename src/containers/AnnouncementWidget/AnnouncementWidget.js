import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import {
  AnnouncementWidgetNextMatch,
  AnnouncementWidgetOnline
} from 'components';
import { MATCH_STATE_NOTSTARTED, MATCH_STATES_ONLINE } from 'helpers/MatchStates';
import { COUNTRY_NAME_RUS } from 'helpers/Countries';
import _ from 'lodash';

@connect(
  state => ({
    schedule: state.schedule.data
  })
)

export default class extends Component {
  static propTypes = {
    durations: PropTypes.object,
    schedule: PropTypes.object
  };

  componentWillMount() {
    this.closestMatch = this.getMatch([MATCH_STATE_NOTSTARTED]);
    if (this.closestMatch) {
      this.closestMatchTimeleft = moment.duration(moment(
        this.closestMatch.date + ' ' + this.closestMatch.time
      ).diff(new Date()));
    }

    this.onlineMatch = this.getMatch(MATCH_STATES_ONLINE);
  }

  getMatch(matchStates) {
    let closestMatch;
    let closestMatchRussia;
    _(this.props.schedule.data).forEach((item) => {
      if (~matchStates.indexOf(item.raw_status)) {
        if (!closestMatch) {
          closestMatch = item;
        }
        if (item.teams[0].name === COUNTRY_NAME_RUS || item.teams[1].name === COUNTRY_NAME_RUS) {
          closestMatchRussia = item;
          return false;
        }
      }
    });
    return closestMatchRussia ? closestMatchRussia : closestMatch;
  }

  render() {
    let widgetType = null;

    if (this.onlineMatch) {
      widgetType = <AnnouncementWidgetOnline match={this.onlineMatch} />;
    } else if (this.closestMatch) {
      widgetType = <AnnouncementWidgetNextMatch match={this.closestMatch} timeleft={this.closestMatchTimeleft}/>;
    }
    return widgetType;
  }
}
