import React, { Component, PropTypes } from 'react';
import moment from 'moment';

export default class EmptyLayout extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired
  };

  render() {
    moment.locale('ru');

    return <div>{this.props.children}</div>;
  }
}
