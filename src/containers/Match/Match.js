import React, { Component, PropTypes } from 'react';
import { asyncConnect } from 'redux-async-connect';
import { connect } from 'react-redux';
import { load, isLoaded } from 'redux/modules/match';
import { NotFound } from 'containers';
import { Match, Meta, Ad } from 'components';
import moment from 'moment';
import { getCountrySlug } from 'helpers/Countries';

@asyncConnect([{
  promise: app => {
    if (isLoaded(app.store.getState(), app.params.id)) {
      return Promise.all();
    }

    const fetcher = load(app.params.id);
    const pr = app.store.dispatch(fetcher);
    return Promise.all([pr]);
  }
}])
@connect(
  state => ({
    matches: state.match.items
  })
)
export default class extends Component {
  static propTypes = {
    params: PropTypes.object,
    matches: PropTypes.object,
    location: PropTypes.object
  };

  render() {
    const id = this.props.params.id;
    const matchState = this.props.matches[id];

    if (!matchState || !matchState.loaded || !matchState.match || matchState.error) {
      return <NotFound />;
    }

    const match = matchState.match;

    const startDate = moment(match.data.date);

    const expectedDate = startDate.locale('en').format('D-MMMM').toLowerCase();
    const realDate = this.props.params.date.toLowerCase();
    if (expectedDate !== realDate) {
      return <NotFound />;
    }

    const expectedTeams = match.data.teams.map(t => getCountrySlug(t.name));
    const realTeams = this.props.params.teams.toLowerCase().split('-');
    if (realTeams.length !== 2 || expectedTeams[0] !== realTeams[0] || expectedTeams[1] !== realTeams[1]) {
      return <NotFound />;
    }

    // todo: restrict matches by tournament id?

    const metaPayload = `${match.data.teams[0].name} ${match.data.teams[1].name} ${startDate.locale('ru').format('D MMMM')}`;

    return (
      <div>
        <Match match={match}>
          <Meta data={{
            title: `ЧМХ 2016 ${metaPayload} - Рамблер`,
            ogTitle: `Чемпионат мира по хоккею 2016: ${metaPayload} ${match.data.stadium.city} ${match.data.stadium.name}`,
            description: `Результаты игры ${metaPayload} ${match.data.stadium.name}`,
            ogDescription: 'Чемпионат мира по хоккею 2016 в России: расписание матчей, онлайн трансляции, результаты, новости',
            keywords: `чемпионат мира по хоккею ${metaPayload} 2016 в ${match.data.stadium.city} ${match.data.stadium.name}`,
            pathname: this.props.location.pathname
          }} />

        </Match>
        <Ad id="superfooter" mobileId="listing" type="footer"/>
      </div>
    );
  }
}
