import React, { Component } from 'react';
import { HttpCodePage } from 'containers';

export default class NotFound extends Component {
  render() {
    return <HttpCodePage code={404} message="Не найдено" />;
  }
}
