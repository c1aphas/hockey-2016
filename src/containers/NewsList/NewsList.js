import React, { Component, PropTypes } from 'react';
import { asyncConnect } from 'redux-async-connect';
import { connect } from 'react-redux';
import { load as loadNews, isLoaded as isNewsLoaded, rollBack } from 'redux/modules/news';
import * as newsActions from 'redux/modules/news';
import { NewsList as NewsListView, Meta, Ad } from 'components';

@asyncConnect([{
  promise: app => {
    const promises = [];

    const state = app.store.getState();

    if (!isNewsLoaded(state)) {
      const pr = app.store.dispatch(loadNews());
      promises.push(pr);
    }

    return Promise.all(promises);
  }
}])
@connect(
  state => ({
    news: state.news.items,
    newsLoading: state.news.loading,
    newsError: state.news.error
  }), {...newsActions, ...{ rollBack: rollBack } }
)
export default class NewsList extends Component {
  static propTypes = {
    news: PropTypes.array,
    dispatch: PropTypes.func,
    newsLoading: PropTypes.bool,
    location: PropTypes.object,
    rollBack: PropTypes.func
  };

  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  componentWillUnmount() {
    this.context.store.dispatch(rollBack);
  }

  getLoadMoreHandler(lastId) {
    return () => this.props.dispatch(loadNews(lastId));
  }

  render() {
    const news = this.props.news;
    const lastItem = news[news.length - 1] || {};
    const isLoading = this.props.newsLoading;

    return (
      <div>
        <Meta data={{
          title: 'Новости чемпионата мира по хоккею 2016 - Рамблер',
          ogTitle: 'Новости чемпионата мира по хоккею 2016',
          description: 'Новости чемпионата мира по хоккею 2016.',
          ogDescription: 'Новости чемпионата мира по хоккею 2016 в России',
          keywords: 'Новости чемпионат мира по хоккею 2016 Результаты скандалы допинг Чехия Швеция Россия Финляндия Канада Франция Беларусь США рейтинговая сетка место результаты счет',
          pathname: this.props.location.pathname,
          robots: 'all'
        }} />

        <NewsListView news={news} loadMore={this.getLoadMoreHandler(lastItem._id)} isLoading={isLoading} />
        <Ad id="superfooter" mobileId="listing" type="footer"/>
      </div>
    );
  }
}
