
require('babel-polyfill');

const basicOpts = {
  api_host: process.env.API_HOST || 'localhost',
  host: '0.0.0.0',
  port: process.env.PORT || '3000',
  app: {
    title: 'Чемпионат мира по хоккею 2016 в России (Санкт-Петербург, Москва) - Рамблер',
    description: 'Чемпионат мира по хоккею 2016. Расписание, новости, турнирная таблица и результаты.',
    tournamentId: '5685975023f74d954a000000',
    // tournamentId: '54a463d04c2941335100006d',
    rootPath: '/hockey-2016/'
  }
};

const envType = process.env.NODE_ENV || 'development';

const environment = {
  development: {
    isProduction: false,
    setupApiProxy: true,
    apiProxyEndpoint: 'http://api.championat.com',
    apiEndpoint: `http://${basicOpts.api_host}:${basicOpts.port}/hockey-2016/api/v3`
  },
  production: {
    isProduction: true,
    apiEndpoint: `http://${basicOpts.api_host}/hockey-2016/api/v3`
  }
}[envType];

const credentials = envType === 'development' ? require(`./config/credentials.${envType}.json`) : {};

module.exports = Object.assign(
  basicOpts,
  environment,
  credentials
);
